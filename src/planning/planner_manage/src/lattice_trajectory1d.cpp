#include "planner_manage/lattice_trajectory1d.h"

namespace polynomial
{
  
LatticeTrajectory1d::LatticeTrajectory1d(std::shared_ptr<Curve1d> ptr_trajectory1d) {
  ptr_trajectory1d_ = ptr_trajectory1d;
}

double LatticeTrajectory1d::get_deriv(int order, double x) const {
  double param_length = ptr_trajectory1d_->ParamLength();
  if (x < param_length) {
    return ptr_trajectory1d_->get_deriv(order, x);
  }

  // do constant acceleration extrapolation;
  // to align all the trajectories with time.
  double p = ptr_trajectory1d_->get_deriv(0, param_length);
  double v = ptr_trajectory1d_->get_deriv(1, param_length);
  double a = ptr_trajectory1d_->get_deriv(2, param_length);

  double t = x - param_length;

  switch (order) {
    case 0:
      return p + v * t + 0.5 * a * t * t;
    case 1:
      return v + a * t;
    case 2:
      return a;
    default:
      return 0.0;
  }
}

double LatticeTrajectory1d::get_y(double x) const {
  return 0.0;
}

double LatticeTrajectory1d::ParamLength() const {
  return ptr_trajectory1d_->ParamLength();
}

// std::string LatticeTrajectory1d::ToString() const {
//   return ptr_trajectory1d_->ToString();
// }

bool LatticeTrajectory1d::has_target_position() const {
  return has_target_position_;
}

bool LatticeTrajectory1d::has_target_velocity() const {
  return has_target_velocity_;
}

bool LatticeTrajectory1d::has_target_time() const { return has_target_time_; }

double LatticeTrajectory1d::target_position() const {
//   ACHECK(has_target_position_);
  return target_position_;
}

double LatticeTrajectory1d::target_velocity() const {
//   ACHECK(has_target_velocity_);
  return target_velocity_;
}

double LatticeTrajectory1d::target_time() const {
//   ACHECK(has_target_time_);
  return target_time_;
}

void LatticeTrajectory1d::set_target_position(double target_position) {
  target_position_ = target_position;
  has_target_position_ = true;
}

void LatticeTrajectory1d::set_target_velocity(double target_velocity) {
  target_velocity_ = target_velocity;
  has_target_velocity_ = true;
}

void LatticeTrajectory1d::set_target_time(double target_time) {
  target_time_ = target_time;
  has_target_time_ = true;
}
} // namespace polynomial
