#include "planner_manage/spline_2d.h"
namespace spline_2d
{

Spline2D::Spline2D(){

}
Spline2D::Spline2D(const std::vector<double> &X, const std::vector<double> &Y){
    set_parameters(X,Y);
}

Spline2D::~Spline2D(){
}

void Spline2D::set_parameters(const std::vector<double> &X, const std::vector<double> &Y){
    calc_s(X,Y,s_);
    spline_x_.set_points(s_,X);
    spline_y_.set_points(s_,Y);
}

void Spline2D::calc_s(const std::vector<double> &x, const std::vector<double> &y, std::vector<double> &s){
    double dx, dy, ds;
    s.push_back(0.0);
    ds = 0;
    for (size_t i = 0; i < x.size()-1; i++){
        dx = x[i+1] - x[i];
        dy = y[i+1] - y[i];
        ds = ds + std::sqrt(dx*dx + dy*dy);
        s.push_back(ds);
    }
}

double Spline2D::calc_k(double s){
    double dx = spline_x_.deriv(1,s);
    double ddx = spline_x_.deriv(2,s);
    double dy = spline_y_.deriv(1,s);
    double ddy = spline_y_.deriv(2,s);

    return (ddy * dx - ddx * dy ) / std::sqrt( pow((dx*dx + dy*dy), 3) );
}

double Spline2D::calc_dk(double s){
    double dx = spline_x_.deriv(1,s);
    double ddx = spline_x_.deriv(2,s);
    double dddx = spline_x_.deriv(3,s);
    double dy = spline_y_.deriv(1,s);
    double ddy = spline_y_.deriv(2,s);
    double dddy = spline_y_.deriv(3,s);

    return ((dddy*dx - dddx*dy)*(dx*dx + dy*dy) - 3.0 * (dx*ddx+dy*ddy)*(ddy*dx - ddx*dy)) / pow((dx*dx + dy*dy), 3);
}

double Spline2D::calc_yaw(double s){
    double dx = spline_x_.deriv(1,s);
    double dy = spline_y_.deriv(1,s);
    return atan2(dy,dx);
}


std::array<double,2> Spline2D::calc_pos(double s){
    std::array<double,2> pos;
    pos[0] = spline_x_(s);
    pos[1] = spline_y_(s);

    return pos;
}

double Spline2D::get_max_s(){
    return s_.back();
}

} // namespace spline_2d