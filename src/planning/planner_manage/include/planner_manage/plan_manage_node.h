#ifndef PLAN_MANAGE_NODE_H
#define PLAN_MANAGE_NODE_H


#include <ros/ros.h>
#include <iostream>
#include <geometry_msgs/Polygon.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose2D.h>
#include "planner_manage/lattice_planner.h"
#include "common_msgs/PlannerCommand.h"
// #include "planner_manage/trajectory_info.h"

class PlanManage
{
private:
    /* data */

    enum EXEC_STATE { INIT, WAIT_TARGET, UPDATE_STATE,  CALC_TRAJ };
    EXEC_STATE exec_state_;

    ros::NodeHandle nh_;
    ros::NodeHandle nh_private_;
    ros::Timer exec_timer_;

    ros::Subscriber path_point_sb;
    ros::Subscriber car_odom_sb;
    ros::Subscriber goal_sb;
    ros::Publisher  local_trajectory_vis_pub;
    ros::Publisher  ref_trajectory_vis_pub;
    ros::Publisher  planner_command_pub;
    ros::Publisher obstacle_vis_pub;

    nav_msgs::Odometry car_odom_;
    nav_msgs::Path local_trajectory_vis_;
    nav_msgs::Path ref_trajectory_vis_;
    geometry_msgs::PoseArray  obstacle_vis_;
    common_msgs::PlannerCommand planner_command_;

    std::vector<double> path_x_, path_y_;
    bool flag_receive_path_points_ = false;
    bool flag_have_goal_ = false;
    bool flag_receive_odom_ = false;
    std::vector<std::vector<double>> obstacle_;
    
    spline_2d::Spline2D ref_path_spline_;

    /**
     * ref_path_[i][0] -- x (cartesian)
     * ref_path_[i][1] -- y (cartesian)
     * ref_path_[i][2] -- s (frenet)
     */
    std::vector<std::vector<double>> ref_path_; 

    frenet_trajectory_info::FrenetTrajectory best_trajectory_;

    // std::vector<TrajectoryPoint> best_trajectory_;

    std::array<double, 3> S0_ = {};
    std::array<double, 3> L0_ = {};
    lp::LatticePlanner lattice_planner_;


    void exec_timer_cb(const ros::TimerEvent &event);
    void path_point_cb(const geometry_msgs::Polygon::ConstPtr &path_point_msg);
    void carOdom_cb(const nav_msgs::Odometry::ConstPtr &odomMsg);
    void goal_cb(const geometry_msgs::PoseStamped::ConstPtr& msg);

    void changeExecState(EXEC_STATE new_state);
    void printExecState();

    void comput_init_frenet_state(const double x, const double y, 
                                  double* ptr_s, double* ptr_d);

    void comput_frenet_state(const double &x, const double &y,
                             const frenet_trajectory_info::FrenetTrajectory &best_trajectory,
                             std::array<double, 3> &S0, std::array<double, 3> &L0);

    void generate_ref_path(const std::vector<double> &path_point_x,
                           const std::vector<double> &path_point_y,
                           std::vector<std::vector<double>> &ref_path);
    /**
     * @brief 在全局范围内 计算车辆当前位置距离参考路径最近点，返回最近点的索引
     * 
     * @param data : 参考路径
     * @param pos  : 车辆位置
     * 
     * @retval 索引值
     */
    size_t findTargetIdx(const std::vector<std::vector<double>> &data, const std::array<double, 2> &pos);
    size_t findTargetIdx(const frenet_trajectory_info::FrenetTrajectory &best_trajectory, const std::array<double, 2> &pos);
    /**
     * @brief 在局部范围内 计算车辆当前位置距离参考路径最近点，返回最近点的索引 (加快搜索速度)
     * 
     * @param data : 参考路径
     * @param pos  : 车辆位置
     * @param last_index  : 上一次的索引值 
     * 
     * @retval 索引值
     */
    size_t findTargetIdx(const std::vector<std::vector<double>> &data, const std::array<double, 2> &pos, const size_t last_index);

public:
    PlanManage(const ros::NodeHandle &nh, const ros::NodeHandle &nh_private);
    ~PlanManage();

    void init();
};




#endif