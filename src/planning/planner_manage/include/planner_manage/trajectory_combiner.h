#ifndef trajectory_combiner_h
#define trajectory_combiner_h

#include <iostream>
#include <vector>
#include <list>
#include <float.h>
#include "polynomial_calculation.h"
#include "spline_2d.h"
#include "common_config.h"
#include "cartesian_frenet_converter.h"
#include "trajectory_info.h"

namespace lp
{

using Curve1d = polynomial::PolynomialCurve1d ;

class TrajectoryCombiner {

 public:

  static void Combine(
      spline_2d::Spline2D& reference_line,
      const Curve1d& lon_trajectory, const Curve1d& lat_trajectory,
      const double &init_relative_time,
      std::vector<TrajectoryPoint>  *combined_trajectory);
};

} // namespace lp


#endif