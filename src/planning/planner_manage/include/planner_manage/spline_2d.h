#ifndef SPLINE_2D_H
#define SPLINE_2D_H

#include <iostream>
#include <vector>
#include <array>
#include <math.h>
#include "spline.h"

namespace spline_2d
{
    
class Spline2D
{
private:
    /* data */
    std::vector<double> s_;

    tk::spline spline_x_, spline_y_;
    void calc_s(const std::vector<double> &x, const std::vector<double> &y, std::vector<double> &s);

public:
    Spline2D();
    Spline2D(const std::vector<double> &X, const std::vector<double> &Y);
    ~Spline2D();

    void set_parameters(const std::vector<double> &X, const std::vector<double> &Y);
    double calc_k(double s);
    double calc_dk(double s);
    double calc_yaw(double s);
    std::array<double,2> calc_pos(double s);
    double get_max_s();
};

} // namespace spline_2d


#endif