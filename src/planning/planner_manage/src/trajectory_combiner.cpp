#include "planner_manage/trajectory_combiner.h"

namespace lp
{

void TrajectoryCombiner::Combine(
      spline_2d::Spline2D& reference_line,
      const Curve1d& lon_trajectory, const Curve1d& lat_trajectory,
      const double &init_relative_time,
	  std::vector<TrajectoryPoint>  *combined_trajectory){

	double s0 = lon_trajectory.get_deriv(0, 0.0);
	double s_ref_max = reference_line.get_max_s();
	double accumulated_trajectory_s = 0.0;

	double last_s = -FLAGS_numerical_epsilon;
	double t_param = 0.0;
	while (t_param < FLAGS_trajectory_time_length) {
		double s = lon_trajectory.get_deriv(0, t_param);

		if (last_s > 0.0) {
			s = std::max(last_s, s);
		}
		last_s = s;

		double s_dot = std::max(FLAGS_numerical_epsilon, lon_trajectory.get_deriv(1, t_param));
		double s_ddot = lon_trajectory.get_deriv(2, t_param);
		if (s > s_ref_max) {
			break;
		}

		double relative_s = s - s0;
		double d = lat_trajectory.get_deriv(0, relative_s);
		double d_prime = lat_trajectory.get_deriv(1, relative_s);
		double d_pprime = lat_trajectory.get_deriv(2, relative_s);

		TrajectoryPoint  trajectory_point;

		// 寻找参考线上最近点
		// apollo 代码中参考线是 std::vector<PathPoint> 不是连续的，因此需要 MatchToPath()
		const double rs = std::min(s, reference_line.get_max_s());

		std::array<double,2> pos = reference_line.calc_pos(rs);
		const double rx = pos[0];
		const double ry = pos[1];
		const double rtheta = reference_line.calc_yaw(rs);
		const double rkappa = reference_line.calc_k(rs);
		const double rdkappa = reference_line.calc_dk(rs);

		std::array<double, 3> s_conditions = {rs, s_dot, s_ddot};
		std::array<double, 3> d_conditions = {d, d_prime, d_pprime};

		common::math::CartesianFrenetConverter::frenet_to_cartesian(
			rs, rx, ry, rtheta, rkappa, rdkappa, s_conditions, d_conditions, 
			&trajectory_point.path_point.x, &trajectory_point.path_point.y, &trajectory_point.path_point.theta, &trajectory_point.path_point.kappa, 
			&trajectory_point.v, &trajectory_point.a);

		trajectory_point.path_point.s = rs;
		trajectory_point.path_point.ds = s_dot;
		trajectory_point.path_point.dds = s_ddot;
		combined_trajectory->emplace_back(trajectory_point);

		t_param = t_param + FLAGS_trajectory_time_resolution;
	}
}
} // namespace lp
