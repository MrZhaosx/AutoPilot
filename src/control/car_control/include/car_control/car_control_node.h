#ifndef CAR_CONTROL_NODE_H
#define CAR_CONTROL_NODE_H

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Float32MultiArray.h>

#include <memory>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <vector>
#include <array>
#include <sstream>
#include <math.h>
#include <string>

#include "../src/controller/stanley_controller.h"
#include <prius_msgs/Control.h>
#include "common_msgs/PlannerCommand.h"

#define NDEBUG 1

class ControlBase
{
private:
    /* data */
    enum EXEC_STATE { INIT, WAIT_TARGET, FIRST_EXEC_TRAJ, EXEC_TRAJ, STOP };

    struct CarState
    {
        double x;
        double y;
        double yaw;
        double v;
    };
    struct PID
    {
        double kp;
        double ki;
        double kd;
        double err;
        double err_last;
        double err_i;
        double out;
    };
    
    ros::NodeHandle nh_;
    ros::NodeHandle nh_private_;
    ros::Timer cmdLoop_timer_;

    ros::Publisher control_cmd_pb_;
    ros::Publisher ref_path_rviz_pb_;
    ros::Subscriber car_odom_sb_;
    ros::Subscriber goal_sb_;
    ros::Subscriber planner_cmd_sb_;

    common_msgs::PlannerCommand planner_cmd_;
    prius_msgs::Control control_cmd_;
    nav_msgs::Odometry car_odom_;
    nav_msgs::Path ref_path_rviz_;

    std::shared_ptr<StanleyController> stanley_controller_ = nullptr;
    double steer_max_, speed_max_;
    double threshold_throttle_break_;
    double target_speed_;

    std::vector<std::array<double,4>> ref_path_;
    std::string file_path_;

    CarState current_state_;
    bool flag_receive_odom_;

    bool flag_have_goal_;
    bool flag_receive_cmd_;

    PID pid_speed_ , pid_steer_;

    EXEC_STATE exec_state_;

#ifdef NDEBUG
    geometry_msgs::PoseStamped debug_target_velocity_;
    ros::Publisher  debug_target_velocity_pb_;


    std_msgs::Float32MultiArray show_multi_array_;
    ros::Publisher  show_multi_array_pb_;
#endif
    void changeExecState(EXEC_STATE new_state);
    void printExecState();

    bool LoadPath(std::string file_path, std::vector<std::array<double,2>> &data);
    void cmdLoop_cb(const ros::TimerEvent &event);
    void carOdom_cb(const nav_msgs::Odometry::ConstPtr &odomMsg);
    void goal_cb(const geometry_msgs::PoseStamped::ConstPtr& msg);
    void planner_cmd_cb(const common_msgs::PlannerCommand::ConstPtr &msg);
    
    void computeControlCmd();
    void computeControlCmd(const std::vector<std::array<double,4>> &ref_path);
    bool stop();
public:
    ControlBase(const ros::NodeHandle &nh, const ros::NodeHandle &nh_private);
    ~ControlBase();

    void init();
};




#endif