#ifndef common_config_h
#define common_config_h

#include <cstdarg>
// #include <iostream>
#include <cassert>
// #define ACHECK(cond) CHECK(cond) << LEFT_BRACKET << MODULE_NAME << RIGHT_BRACKET

#define COUT_ERROR  std::cerr << "Error : " << __FILE__ << " : in function " << __func__  << " : at line  " << __LINE__ << std::endl;

#define FLAGS_trajectory_time_resolution 0.2
#define FLAGS_trajectory_time_length 8
#define FLAGS_polynomial_minimal_param 0.5

#define FLAGS_longitudinal_acceleration_upper_bound 2.0       // 最大 加速 加速度
#define FLAGS_longitudinal_acceleration_lower_bound -2.0      // 最大 减速 加速度
// 在某一时间点采样间隙
#define FLAGS_min_velocity_sample_gap FLAGS_longitudinal_acceleration_upper_bound/2.0                       
#define FLAGS_num_velocity_sample 6                             // 在某一时间点采样个数


#define FLAGS_default_cruise_speed 15.0/3.6                     // 默认的巡航速度

#define FLAGS_speed_upper_bound 30.0/3.6
#define FLAGS_speed_lower_bound 0.0


#define FLAGS_longitudinal_jerk_upper_bound 5.0
#define FLAGS_longitudinal_jerk_lower_bound -5.0

#define FLAGS_lateral_acceleration_bound 1.0
#define FLAGS_lateral_jerk_bound 0.5



#define FLAGS_numerical_epsilon 0.0000001                       // epsilon

// constraint_checker.cpp
#define FLAGS_kappa_bound 1.0

/* ************ 权重参数  trajectory_evaluator.cpp ************ */
#define FLAGS_weight_lon_objective              1.0
#define FLAGS_weight_lon_jerk                   1.0
#define FLAGS_weight_lon_collision              1.0
#define FLAGS_weight_centripetal_acceleration   1.0
#define FLAGS_weight_lat_offset                 1.0
#define FLAGS_weight_lat_comfort                1.0

// LonObjectiveCost
#define FLAGS_weight_target_speed               1.0
#define FLAGS_weight_dist_travelled             10.0

// CentripetalAccelerationCost
#define FLAGS_speed_lon_decision_horizon        30.0   // 横向轨迹的纵向评估范围
#define FLAGS_trajectory_space_resolution       0.5     // 分辨率

// LatOffsetCost
#define FLAGS_lat_offset_bound                  1.5   // 横向边界
#define FLAGS_weight_opposite_side_offset       10.0  // 不同侧
#define FLAGS_weight_same_side_offset           1.0   // 同侧

// #define FLAGS_trajectory_space_resolution       0.0   // x
// #define FLAGS_trajectory_space_resolution       0.0   // x


#endif