#include "planner_manage/constraint_checker.h"

namespace lp
{

template <typename T>
bool WithinRange(const T v, const T lower, const T upper) {
    return lower <= v && v <= upper;
}

ConstraintChecker::Result ConstraintChecker::ValidTrajectory(const std::vector<TrajectoryPoint>& trajectory){

    const double kMaxCheckRelativeTime = FLAGS_trajectory_time_length;
    for (const auto& p : trajectory){
        double lon_v = p.v;
        if (!WithinRange(lon_v, FLAGS_speed_lower_bound, FLAGS_speed_upper_bound)) {
            std::cout << " exceeds bound, value: " << lon_v << ", bound ["
                    << FLAGS_speed_lower_bound << ", " 
                    << FLAGS_speed_upper_bound << "]."
                    << std::endl;
            return Result::LON_VELOCITY_OUT_OF_BOUND;
        }

        double lon_a = p.a;
        if (!WithinRange(lon_a, FLAGS_longitudinal_acceleration_lower_bound,
                     FLAGS_longitudinal_acceleration_upper_bound)) {
            std::cout << " exceeds bound, value: " << lon_a << ", bound ["
                    << FLAGS_longitudinal_acceleration_lower_bound << ", "
                    << FLAGS_longitudinal_acceleration_upper_bound << "]."
                    << std::endl;
            return Result::LON_ACCELERATION_OUT_OF_BOUND;
        }
        double kappa = p.path_point.kappa;
        if (!WithinRange(kappa, -FLAGS_kappa_bound,FLAGS_kappa_bound)) {
            std::cout << " exceeds bound, value: " << kappa << ", bound ["
                    << -FLAGS_kappa_bound << ", "
                    << FLAGS_kappa_bound << "]."
                    << std::endl;
            return Result::CURVATURE_OUT_OF_BOUND;
        }

    }

    // lon_jerk lon_a check



    return Result::VALID;
}

} // namespace lp
