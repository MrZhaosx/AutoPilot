#include "pid_controller.h"

namespace controller
{

PIDController::PIDController(double steer_max, double velocity_max)
    :   steer_max_(steer_max),
        velocity_max_(velocity_max)
{
}

PIDController::~PIDController()
{
}

void PIDController::compute_control_command(const common_msgs::PlannerCommand &planner_cmd, 
                             const std::array<double,4> &current_state,
                              std::array<double,2> &out){
    
    pid_yaw_.err = planner_cmd.yaw - current_state[2];
    pid_yaw_.out = pid_yaw_.kp * pid_yaw_.err + pid_yaw_.kd * (pid_yaw_.err - pid_yaw_.err_last);
    pid_yaw_.err_last = pid_yaw_.err;

    pid_velocity_.err = planner_cmd.velocity.x - current_state[3];
    pid_velocity_.out = pid_velocity_.kp * pid_velocity_.err + pid_velocity_.kd * (pid_velocity_.err - pid_velocity_.err_last);
    pid_velocity_.err_last = pid_velocity_.err;
}

void PIDController::limit_fun(double &data, const double amplitude){
    if(data > std::abs(amplitude))
        data = std::abs(amplitude);
    else if(data < -std::abs(amplitude))
        data = -std::abs(amplitude);
}


} // namespace controller
