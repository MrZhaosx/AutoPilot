#include "planner_manage/lattice_planner.h"

namespace lp
{

LatticePlanner::LatticePlanner(/* args */){
}

LatticePlanner::~LatticePlanner(){
}

/**
 * 
 * 
 */ 
std::vector<Condition> LatticePlanner::SampleLonEndConditionsForCruising(const std::array<double, 3> &S0, const double &ref_cruise_speed){
    static constexpr size_t num_of_time_samples = 9;
    std::array<double, num_of_time_samples> time_samples;
    for (size_t i = 1; i < num_of_time_samples; ++i) {
        auto ratio = static_cast<double>(i) / static_cast<double>(num_of_time_samples - 1);
        time_samples[i] = FLAGS_trajectory_time_length * ratio;
    }
    time_samples[0] = FLAGS_polynomial_minimal_param;

    std::vector<Condition> end_s_conditions;
    for (const auto& time : time_samples) {

        double v_upper = S0[1] + FLAGS_longitudinal_acceleration_upper_bound * time;
        v_upper = std::min(v_upper, ref_cruise_speed);

        double v_lower = S0[1] + FLAGS_longitudinal_acceleration_lower_bound * time;
        v_lower = std::max(v_lower, 0.0);

        State lower_end_s = {0.0, v_lower, 0.0};
        end_s_conditions.emplace_back(lower_end_s, time);

        State upper_end_s = {0.0, v_upper, 0.0};
        end_s_conditions.emplace_back(upper_end_s, time);

        double v_range = v_upper - v_lower;
        // Number of sample velocities
        size_t num_of_mid_points =
            std::min(static_cast<size_t>(FLAGS_num_velocity_sample - 2),
                    static_cast<size_t>(v_range / FLAGS_min_velocity_sample_gap));

        if (num_of_mid_points > 0) {
            double velocity_seg = v_range / static_cast<double>(num_of_mid_points + 1);
            for (size_t i = 1; i <= num_of_mid_points; ++i) {
                State end_s = {0.0, v_lower + velocity_seg * static_cast<double>(i), 0.0};
                end_s_conditions.emplace_back(end_s, time);
            }
        }
    }
    return end_s_conditions;
}

std::vector<Condition> LatticePlanner::SampleLatEndConditions(){
    std::vector<Condition> end_d_conditions;
    std::array<double, 7> end_d_candidates = {-1.5, -1, -0.5, 0.0, 0.5, 1.0, 1.5};
    std::array<double, 4> end_s_candidates = {5.0, 10.0, 20.0, 30.0};

    for (const auto& s : end_s_candidates) {
        for (const auto& d : end_d_candidates) {
            State end_d_state = {d, 0.0, 0.0};
            end_d_conditions.emplace_back(end_d_state, s);
        }
    }
    return end_d_conditions;
}

void LatticePlanner::GenerateLongitudinalTrajectoryBundle(const std::array<double, 3> &S0, const double &ref_cruise_speed, 
                                                          Trajectory1DBundle *ptr_lon_trajectory_bundle){
    std::vector<Condition> end_conditions = SampleLonEndConditionsForCruising(S0,ref_cruise_speed);                                                           
    if (end_conditions.empty()) {
        std::cout << "GenerateLongitudinalTrajectoryBundle end_conditions is empty" << std::endl;
        return;
    }
    ptr_lon_trajectory_bundle->reserve(ptr_lon_trajectory_bundle->size() + end_conditions.size());
    for (const auto& end_condition : end_conditions){
        auto ptr_trajectory1d = std::make_shared<polynomial::LatticeTrajectory1d>(
                                    std::shared_ptr<polynomial::PolynomialCurve1d>(new polynomial::QuarticPoly(
                                        S0,end_condition.first,end_condition.second)));
        ptr_lon_trajectory_bundle->push_back(ptr_trajectory1d);
    }
}

void LatticePlanner::GenerateLateralTrajectoryBundle(const std::array<double, 3> &L0, Trajectory1DBundle *ptr_lat_trajectory_bundle){

    auto end_conditions = SampleLatEndConditions();

    if (end_conditions.empty()) {
        std::cout << "GenerateLateralTrajectoryBundle end_conditions is empty" << std::endl;
        return;
    }

    ptr_lat_trajectory_bundle->reserve(ptr_lat_trajectory_bundle->size() + end_conditions.size());
    for (const auto& end_condition : end_conditions){
        auto ptr_trajectory1d =std::make_shared<polynomial::LatticeTrajectory1d>(
                                    std::shared_ptr<polynomial::PolynomialCurve1d>(new polynomial::QuinticPoly(
                                        L0,end_condition.first,end_condition.second)));
        ptr_lat_trajectory_bundle->push_back(ptr_trajectory1d);
    }
}

bool LatticePlanner::PlanOnReferenceLine(const std::array<double, 3> &S0, const std::array<double, 3> &L0, const double &planning_target, spline_2d::Spline2D &reference_line,
                                         std::vector<TrajectoryPoint> &best_trajectory){

  // 5. generate 1d trajectory bundle for longitudinal and lateral respectively.
    std::vector<std::shared_ptr<Curve1d>> lon_trajectory1d_bundle;
    std::vector<std::shared_ptr<Curve1d>> lat_trajectory1d_bundle;
    GenerateLongitudinalTrajectoryBundle(S0, planning_target, &lon_trajectory1d_bundle);
    GenerateLateralTrajectoryBundle(L0, &lat_trajectory1d_bundle);

  // 6. first, evaluate the feasibility of the 1d trajectories according to
  // dynamic constraints.
  //   second, evaluate the feasible longitudinal and lateral trajectory pairs
  //   and sort them according to the cost.

    TrajectoryEvaluator trajectory_evaluator(S0, planning_target, 
                                            lon_trajectory1d_bundle, lat_trajectory1d_bundle,
                                            reference_line);

  // 7. always get the best pair of trajectories to combine; return the first
  // collision-free trajectory.
    size_t constraint_failure_count = 0;
    size_t collision_failure_count = 0;
    size_t combined_constraint_failure_count = 0;

    size_t lon_vel_failure_count = 0;
    size_t lon_acc_failure_count = 0;
    size_t lon_jerk_failure_count = 0;
    size_t curvature_failure_count = 0;
    size_t lat_acc_failure_count = 0;
    size_t lat_jerk_failure_count = 0;

    size_t num_lattice_traj = 0;

    // std::vector<lp::TrajectoryPoint> combined_trajectory;
    double init_relative_time = 0.0;

    while (trajectory_evaluator.has_more_trajectory_pairs()){
        double trajectory_pair_cost = trajectory_evaluator.top_trajectory_pair_cost();
        auto trajectory_pair = trajectory_evaluator.next_top_trajectory_pair();

        // combine two 1d trajectories to one 2d trajectory
        TrajectoryCombiner::Combine(reference_line, *trajectory_pair.first, *trajectory_pair.second, init_relative_time, 
                                    &best_trajectory);


        auto result = ConstraintChecker::ValidTrajectory(best_trajectory);
        if (result != ConstraintChecker::Result::VALID) {
            ++combined_constraint_failure_count;

            switch (result) {
                case ConstraintChecker::Result::LON_VELOCITY_OUT_OF_BOUND:
                    lon_vel_failure_count += 1;
                    break;
                case ConstraintChecker::Result::LON_ACCELERATION_OUT_OF_BOUND:
                    lon_acc_failure_count += 1;
                    break;
                case ConstraintChecker::Result::LON_JERK_OUT_OF_BOUND:
                    lon_jerk_failure_count += 1;
                    break;
                case ConstraintChecker::Result::CURVATURE_OUT_OF_BOUND:
                    curvature_failure_count += 1;
                    break;
                case ConstraintChecker::Result::LAT_ACCELERATION_OUT_OF_BOUND:
                    lat_acc_failure_count += 1;
                    break;
                case ConstraintChecker::Result::LAT_JERK_OUT_OF_BOUND:
                    lat_jerk_failure_count += 1;
                    break;

                default:
                    // Intentional empty
                    break;
            }
            continue;
        }

        // check collision with other obstacles
        // ...

        num_lattice_traj += 1;
        // best_trajectory = combined_trajectory;

        break;
    }
    
    if(num_lattice_traj>0){
        return true;
    } else {
        return false;
    }
}

void LatticePlanner::calc_frenet_trajectories(std::array<double, 3> &S0, std::array<double, 3> &L0){

    polynomial::QuinticPoly lat_poly5;    // 五次多项式
    polynomial::QuarticPoly lon_poly4;    // 四次多项式
    std::array<double, 3> S_i = {0.0, 0.0, 0.0};
    std::array<double, 3> L_i = {0.0, 0.0, 0.0};

    /*  横向规划  */
    for (double li = MIN_ROAD_WIDTH_; li <= MAX_ROAD_WIDTH_; li = li + D_ROAD_WIDTH_){
        for (double ti = MIN_T_; ti <= MAX_T_; ti = ti + D_T_){
            L_i[0] = li;
            lat_poly5.set_parameters(L0,L_i,ti);

            // 横向插值
            frenet_trajectory_.clear();
            for (double i = 0; i <= ti; i = i + D_T_){
                frenet_trajectory_.l.push_back(lat_poly5.get_y(i));
                frenet_trajectory_.dl.push_back(lat_poly5.get_deriv(1,i));
                frenet_trajectory_.ddl.push_back(lat_poly5.get_deriv(2,i));
                frenet_trajectory_.dddl.push_back(lat_poly5.get_deriv(3,i));
            }
            
            /*  纵向规划  */
            for (double ds = TARGET_SPEED_ - D_TARGET_SPEED_ * N_S_SAMPLE_ ; ds < TARGET_SPEED_ + D_TARGET_SPEED_ * N_S_SAMPLE_; ds = ds + D_TARGET_SPEED_){
                
                S_i[1] = ds;
                lon_poly4.set_parameters(S0,S_i,ti);

                // 纵向插值
                for (double i = 0; i <= ti; i = i + D_T_){
                    frenet_trajectory_.s.push_back(lon_poly4.get_y(i));
                    frenet_trajectory_.ds.push_back(lon_poly4.get_deriv(1,i));
                    frenet_trajectory_.dds.push_back(lon_poly4.get_deriv(2,i));
                    frenet_trajectory_.ddds.push_back(lon_poly4.get_deriv(3,i));

                    frenet_trajectory_.l_dot.push_back(frenet_trajectory_.dl[i] / frenet_trajectory_.ds[i]);
                }
                
                // Square of lateral jerk
                double Jl = 0;
                for (size_t i = 0; i < frenet_trajectory_.dddl.size(); i++){
                    Jl = Jl + frenet_trajectory_.dddl[i] * frenet_trajectory_.dddl[i];
                }
                //  Square of longitudinal jerk
                double Js = 0;
                for (size_t i = 0; i < frenet_trajectory_.ddds.size(); i++){
                    Js = Js + frenet_trajectory_.ddds[i] * frenet_trajectory_.ddds[i];
                }

                // Square of diff from target speed
                double dv = (TARGET_SPEED_ - frenet_trajectory_.ds.back()) * (TARGET_SPEED_ - frenet_trajectory_.ds.back());

                // calculate cost
                frenet_trajectory_.Jl = KJ * Jl + KT * ti + KL * frenet_trajectory_.l.back()*frenet_trajectory_.l.back();
                frenet_trajectory_.Js = KJ * Js + KT * ti + KV * dv;
                frenet_trajectory_.J = KLAT * frenet_trajectory_.Jl + KLON * frenet_trajectory_.Js;

                frenet_trajectory_all_.push_back(frenet_trajectory_);
            }
        }
    }
}

void LatticePlanner::calc_global_trajectories(std::list<frenet_trajectory_info::FrenetTrajectory> &frenet_trajectory_all, spline_2d::Spline2D &reference_path){
    auto it = frenet_trajectory_all.begin();
    while(it != frenet_trajectory_all.end()){
        std::array<double,2> pos_i;
        frenet_trajectory_info::FrenetTrajectory &frenet_trajectory = *it;
        for (size_t j = 0; j < frenet_trajectory.s.size(); j++){
            if(frenet_trajectory.s[j] > reference_path.get_max_s())
                break;   
                 
            pos_i = reference_path.calc_pos(frenet_trajectory.s[j]);    //计算全局位置
            double yaw_i = reference_path.calc_yaw(frenet_trajectory.s[j]);
            double l_i = frenet_trajectory.l[j];
            double fx = pos_i[0] - l_i * sin(yaw_i);
            double fy = pos_i[1] + l_i * cos(yaw_i);
            frenet_trajectory.x.push_back(fx);
            frenet_trajectory.y.push_back(fy);

            // double rs_i = frenet_trajectory.s[j];  
            // double rk_i = reference_path.calc_k(frenet_trajectory.s[j]);
            // double rdk_i = reference_path.calc_dk(frenet_trajectory.s[j]);
            // std::array<double, 3> s_condition = {frenet_trajectory.s[j], frenet_trajectory.ds[j], frenet_trajectory.dds[j]};
            // std::array<double, 3> d_condition = {frenet_trajectory.l[j], frenet_trajectory.l_dot[j], frenet_trajectory.l_ddot[j]};
            // double ptr_x, ptr_y, ptr_theta, tr_kappa, ptr_v, ptr_a;
            // cartesian_frenet_converter_.frenet_to_cartesian(rs_i,pos_i[0],pos_i[1],yaw_i,rk_i,rdk_i,s_condition,d_condition,
            //                                                 &ptr_x, &ptr_y, &ptr_theta, &tr_kappa, &ptr_v, &ptr_a);
            // frenet_trajectory.x.push_back(ptr_x);
            // frenet_trajectory.y.push_back(ptr_y);
            // frenet_trajectory.theta.push_back(ptr_theta);
            // frenet_trajectory.kappa.push_back(tr_kappa);
            // frenet_trajectory.v.push_back(ptr_v);
            // frenet_trajectory.a.push_back(ptr_a);

// 待修改
            double kr = reference_path.calc_k(frenet_trajectory.s[j]);
            double ds = frenet_trajectory.ds[j];
            double l_dot = frenet_trajectory.l_dot[j];    // l_dot : dl/ds      dl : dl/dt
            double v = std::sqrt( std::pow(ds*(1-l_i*kr),2) + std::pow(ds*l_dot, 2) );
            frenet_trajectory.v.push_back(v);
        }
        if(frenet_trajectory.x.size() <= 1){ // --------------------------------------------------  <= or ==
            std::cout << "frenet_trajectory.x.size() <= 1" << std::endl;
            break;
        }
        // 计算kappa
        for (size_t k = 0; k < frenet_trajectory.x.size() - 1; k++){
            double dx = frenet_trajectory.x[k+1] - frenet_trajectory.x[k];
            double dy = frenet_trajectory.y[k+1] - frenet_trajectory.y[k];
            frenet_trajectory.theta.push_back( atan2(dy,dx) );
            frenet_trajectory.dL.push_back(sqrt(dx*dx + dy*dy));
        }
        frenet_trajectory.theta.push_back(frenet_trajectory.theta.back());
        
        // 计算曲率 -- 切线转角与该弧长之比  
        for (size_t n = 0; n < frenet_trajectory.dL.size(); n++){
            double kappa = (frenet_trajectory.theta[n+1] - frenet_trajectory.theta[n]) / frenet_trajectory.dL[n];
            frenet_trajectory.kappa.push_back(kappa);
        }
        frenet_trajectory.kappa.push_back(frenet_trajectory.kappa.back());
        it++;
    }
}

void LatticePlanner::check_trajectories(std::list<frenet_trajectory_info::FrenetTrajectory> &frenet_trajectory_all, std::vector<std::vector<double>> &obstacle){
    static int num=0;
    num = 0;
    auto it = frenet_trajectory_all.begin();
    while(it != frenet_trajectory_all.end()){
        const frenet_trajectory_info::FrenetTrajectory &ft = *it;
        bool check_fail = false;
        for (size_t j = 0; j < ft.ds.size(); j++){
            if (ft.ds[j] > MAX_SPEED_){                 // Max speed check
                // std::cout << "MAX_SPEED_ : " << num << "  " << ft.ds[j] << std::endl;
                break;
            }
            if (abs(ft.dds[j]) > MAX_ACCEL_){           // Max accleration check
                // std::cout << "MAX_ACCEL_ : " << num << "  " << ft.dds[j]  << std::endl;
                break;
            }
            if (j < ft.kappa.size())
                if (abs(ft.kappa[j]) > MAX_CURVATURE_){  // Max curvature check
                    // std::cout << "MAX_CURVATURE_ : " << num << "  " << ft.kappa[j]  << std::endl;
                    break;
                }
            if (check_collision(ft,obstacle)){          // obstacle check
                break;
            }
            frenet_trajectory_ok_.push_back(ft);
        }
        it++;
        num++ ;
    }
    // std::cout << "frenet_trajectory_ok_ size : " << frenet_trajectory_ok_.size() << std::endl;
}

bool LatticePlanner::check_collision(const frenet_trajectory_info::FrenetTrajectory &ft, std::vector<std::vector<double>> &obstacle){
    double min_dis = ROBOT_RADIUS_ * ROBOT_RADIUS_;
    for (size_t i = 0; i < obstacle.size(); i++){
        double ox = obstacle[i][0];
        double oy = obstacle[i][1];
        for (size_t j = 0; j < ft.x.size(); j++){
            double xj = ft.x[j];
            double yj = ft.y[j];
            double dis = (ox - xj)*(ox - xj) + (oy - yj)*(oy - yj);
            if(dis <= min_dis)
                return true;
        }
    }
    return false;
}

bool LatticePlanner::lattice_planning(std::array<double, 3> &S0, 
                          std::array<double, 3> &L0,
                          spline_2d::Spline2D &reference_path,
                          std::vector<std::vector<double>> &obstacle,
                          frenet_trajectory_info::FrenetTrajectory &best_trajectory){
    // 清除上次规划保留的轨迹
    frenet_trajectory_all_.clear();
    frenet_trajectory_ok_.clear();

    calc_frenet_trajectories(S0,L0);
    calc_global_trajectories(frenet_trajectory_all_,reference_path);
    check_trajectories(frenet_trajectory_all_,obstacle);

    if(frenet_trajectory_ok_.size() ==0 ){      //  没有轨迹
        return false;
    }
    double mincost = DBL_MAX;
    auto best_trajectory_index = frenet_trajectory_ok_.begin();
    auto it = frenet_trajectory_ok_.begin();
    while(it != frenet_trajectory_ok_.end()){
        const frenet_trajectory_info::FrenetTrajectory &ft = *it;
        if( mincost > ft.J){
            mincost = ft.J;
            best_trajectory_index = it;
        }
        it++;
    }
    best_trajectory = *best_trajectory_index;
    return true;
}


} // namespace lp

/* LatticePlanner 编译测试  */
// int main(int, char **)
// {
//     std::cout << "hello " << std::endl;
//     return 0;
// }