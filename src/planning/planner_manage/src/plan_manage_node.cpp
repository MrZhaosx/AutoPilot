#include <ros/ros.h>
#include "planner_manage/spline_2d.h"
#include "planner_manage/plan_manage_node.h"
#include <chrono>

PlanManage::PlanManage(const ros::NodeHandle &nh, const ros::NodeHandle &nh_private)
    : nh_(nh),
      nh_private_(nh_private){
}

PlanManage::~PlanManage(){
}

void PlanManage::init(){

    std::cout << "start init " << std::endl;

    std::vector<double> X = {5.1866667, 11.9266667, 18.1544444, 23.4155556, 27.4722222, 30.4694444,           // 1

                            32.8752778, 32.8752778, 32.8752778, 32.8752778, 32.8752778, 32.8752778, 32.8752778,         // 2

                            31.8752778, 27.9917761, 22.4075181, 15.4505918, 7.5837827, -1.2177976, -11.2284192,         // 3
                            -22.4729269, -34.7591410, -47.1708984, -58.3050953, -66.9225847, -73.0044914, -77.5098540, -81.4820034,

                            -81.4820034, -81.4820034, -81.4820034, -81.4820034, -81.4820034, -81.4820034, -81.4820034,  // 4

                             -79.6300000, -77.3511111, -74.2833333, -70.4572222,               // 5

                             -51.1913889, -36.1913889, -24.1913889, -10.1913889, 4.8               // 6
                            }; 

    std::vector<double> Y = {-9.0, -10.4933333, -12.9533333, -16.2666667, -20.6355556, -25.8211111, 
                            
                            -38.5044444, -59.0,      -80.0,       -101.1,       -122.2,     -143.1,     -163.1,

                            -180.1654450, -193.3520454, -205.3876507, -215.18271, -222.92833, -228.828867, -233.187460,
                            -236.232852, -237.589067, -236.372241, -231.5521104, -222.686172, -210.170538, -195.432075, -179.8276784,

                            -163.1,     -143.1,     -122.2,     -101.1,     -80.0,       -59.0,      -38.5044444,

                             -22.2255556, -18.6766667, -15.3185122, -12.2242372,

                             -9.0, -9.0, -9.0, -9.0, -9.0
                            };
    generate_ref_path(X,Y,ref_path_);     

    std::vector<std::vector<double>> ob = {{32.8752778, -82.0},{32.8752778, -143.1}};
    // std::vector<std::vector<double>> ob = {{4.8,-9}};
    obstacle_ = ob;

    std::cout << "obstacle_ size : " << obstacle_.size() << std::endl;

    geometry_msgs::Pose obstacle_pos;
    for (size_t i = 0; i < obstacle_.size(); i++){
        obstacle_pos.position.x = obstacle_[i][0];
        obstacle_pos.position.y = obstacle_[i][1];
        obstacle_pos.position.z = 0.2;
        obstacle_vis_.poses.push_back(obstacle_pos);
    }
    
    exec_timer_ = nh_.createTimer(ros::Duration(0.1), &PlanManage::exec_timer_cb,this); // Define timer for constant loop rate
    path_point_sb = nh_.subscribe<geometry_msgs::Polygon>("/path/point", 1, &PlanManage::path_point_cb, this);
    car_odom_sb = nh_.subscribe<nav_msgs::Odometry>("/prius/odom", 1, &PlanManage::carOdom_cb, this);
    planner_command_pub = nh_.advertise<common_msgs::PlannerCommand>("/planner/command",1);
    local_trajectory_vis_pub = nh_.advertise<nav_msgs::Path>("/vis/local/trajectory", 1);
    ref_trajectory_vis_pub = nh_.advertise<nav_msgs::Path>("/vis/reference/trajectory", 1);
    goal_sb = nh_.subscribe<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, &PlanManage::goal_cb, this);
    obstacle_vis_pub = nh_.advertise<geometry_msgs::PoseArray>("/vis/obstacle", 1);

    std::cout << "init ok !!!" << std::endl;
}

void PlanManage::exec_timer_cb(const ros::TimerEvent &event){
    static int fsm_num = 0;
    switch (exec_state_){
        case INIT :{
            if( !flag_receive_odom_){
                fsm_num++;
                if (fsm_num == 100) {
                    printExecState();
                    fsm_num = 0;
                    std::cout << "no odom !!! " << std::endl;
                }
                // return;
            }
            changeExecState(WAIT_TARGET);
            break;
        }
        case WAIT_TARGET :{
            fsm_num++;
            if (fsm_num == 10) {
                printExecState();
                fsm_num = 0;
                ref_trajectory_vis_.header.stamp = ros::Time::now();
                ref_trajectory_vis_.header.frame_id = "map";
                ref_trajectory_vis_pub.publish(ref_trajectory_vis_);

                obstacle_vis_.header.stamp = ros::Time::now();
                obstacle_vis_.header.frame_id = "map";
                obstacle_vis_pub.publish(obstacle_vis_);
            }
            if( !flag_have_goal_){
                return;
            }
            // 寻找轨迹上最近的点在frenet坐标系上的 s,l 
            double l0,s0;
            comput_init_frenet_state(car_odom_.pose.pose.position.x, car_odom_.pose.pose.position.y,
                                      &s0, &l0);
            std::array<double, 3> S0 = {2.0, 1.0, 0.0};
            std::array<double, 3> L0 = {1.0, 0.0, 0.0};
            // S0_ = {s0,0,0};
            // L0_ = {(double)(int)l0,0,0};
            S0_ = S0;
            L0_ = L0;
            std::cout << "S0_  " << S0_[0] << "   " << S0_[1] << "   " << S0_[2] << "   " << std::endl;
            std::cout << "L0_  " << L0_[0] << "   " << L0_[1] << "   " << L0_[2] << "   " << std::endl;
            changeExecState(CALC_TRAJ);
            break;
        }
        case UPDATE_STATE :{

            // 更新状态
            /* 如果与实际误差太大，需要处理一下 */
            comput_frenet_state(car_odom_.pose.pose.position.x, car_odom_.pose.pose.position.y,best_trajectory_,S0_,L0_);
            exec_state_ = EXEC_STATE::CALC_TRAJ;
            break;
        }
        case CALC_TRAJ :{
            
            std::chrono::steady_clock::time_point t1  = std::chrono::steady_clock::now();

            // best_trajectory_.clear();
            // bool planner_ok = lattice_planner_.lattice_planning(S0_,L0_,ref_path_spline_,obstacle_, best_trajectory_);
            
            best_trajectory_.clear();
            std::vector<lp::TrajectoryPoint> trajectory_point;
            bool planner_ok = lattice_planner_.PlanOnReferenceLine(S0_, L0_, FLAGS_default_cruise_speed, ref_path_spline_, trajectory_point);

            std::chrono::steady_clock::time_point t2  = std::chrono::steady_clock::now();
            std::chrono::duration<double> time_used = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);
            std::cout << "planner time_used : " << time_used.count() << std::endl;

            if(!planner_ok){
                std::cout << "planner failed !!! " << std::endl;
                std::cout << "best_trajectory_.s size : " << best_trajectory_.s.size() << std::endl;
                return;
            }

            for (size_t i = 0; i < trajectory_point.size(); i++){
                best_trajectory_.x.push_back(trajectory_point[i].path_point.x);
                best_trajectory_.y.push_back(trajectory_point[i].path_point.y);
                best_trajectory_.theta.push_back(trajectory_point[i].path_point.theta);
                best_trajectory_.v.push_back(trajectory_point[i].v);
                best_trajectory_.s.push_back(trajectory_point[i].path_point.s);
                best_trajectory_.ds.push_back(trajectory_point[i].path_point.ds);
                best_trajectory_.dds.push_back(trajectory_point[i].path_point.dds);

            }


            geometry_msgs::Pose2D pose2d;
            planner_command_.pose2d.clear();
            planner_command_.velocity.clear();
            for (size_t i = 0; i < best_trajectory_.x.size(); i++){
                pose2d.x = best_trajectory_.x[i];
                pose2d.y = best_trajectory_.y[i];
                pose2d.theta = best_trajectory_.theta[i];
                planner_command_.pose2d.push_back(pose2d);
                planner_command_.velocity.push_back(best_trajectory_.v[i]);
            }

            // std::cout << "best_trajectory_.v[0] is : " << best_trajectory_.v[0] << std::endl;
            std::cout << "best_trajectory_.x size : " << best_trajectory_.x.size() << "  "
                                                      << best_trajectory_.y.size() << "  "
                                                      << best_trajectory_.theta.size() << "  "
                                                      << best_trajectory_.v.size() << "  "
                                                      << std::endl;
            // rviz 显示轨迹
            local_trajectory_vis_.poses.clear();
            geometry_msgs::PoseStamped pos;
            pos.pose.position.z = 0.1;

            for (size_t i = 0; i < best_trajectory_.x.size(); i++){
                pos.pose.position.x = best_trajectory_.x[i];
                pos.pose.position.y = best_trajectory_.y[i];
                local_trajectory_vis_.poses.push_back(pos);
            }

            ros::Time time_now = ros::Time::now();
            planner_command_.header.stamp = time_now;
            planner_command_pub.publish(planner_command_);

            local_trajectory_vis_.header.stamp = time_now;
            local_trajectory_vis_.header.frame_id = "map";
            local_trajectory_vis_pub.publish(local_trajectory_vis_);
            
            exec_state_ = EXEC_STATE::UPDATE_STATE;
            break;
        }


        default:
            break;
    }


}

void PlanManage::goal_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
    flag_have_goal_ = true;
}

void PlanManage::path_point_cb(const geometry_msgs::Polygon::ConstPtr &path_point_msg){
    for (size_t i = 0; i < path_point_msg->points.size(); i++){
        path_x_.clear();
        path_y_.clear();
        path_x_.push_back(path_point_msg->points[i].x);
        path_y_.push_back(path_point_msg->points[i].y);
    }
    flag_receive_path_points_ = true;
}

void PlanManage::generate_ref_path(const std::vector<double> &path_point_x,
                                   const std::vector<double> &path_point_y,
                                   std::vector<std::vector<double>> &ref_path){
    // 根据路径点计算参考轨迹 3次样条曲线参数 
    ref_path_spline_.set_parameters(path_point_x, path_point_y);
    // 生成参考轨迹插值点，用于计算距离车辆最近的路径点
    geometry_msgs::PoseStamped this_pose_stamped; 
    ref_path.clear();
    for (double s = 0; s < ref_path_spline_.get_max_s(); s = s + 0.5){
        std::array<double,2> pos = ref_path_spline_.calc_pos(s);

        this_pose_stamped.pose.position.x = pos[0];
        this_pose_stamped.pose.position.y = pos[1];
        this_pose_stamped.pose.position.z = 0.1;
        ref_trajectory_vis_.poses.push_back(this_pose_stamped); 

        std::vector<double> pos_s = {pos[0], pos[1], s};
        // pos.push_back(s);
        ref_path.push_back(pos_s);
    }
    // std::cout << "ref_path_  size : " << ref_path.size() << std::endl;
}

size_t PlanManage::findTargetIdx(const std::vector<std::vector<double>> &data, const std::array<double, 2> &pos){
	std::vector<double> dis;
    size_t min_dis_index = 0;
	int data_size = data.size();
	for (size_t i = 0; i < data_size; i++)
		dis.push_back(pow((data[i][0] - pos[0]),2) + pow((data[i][1] - pos[1]),2));
	
	double min_dis = dis[min_dis_index];
	for (size_t i = min_dis_index ; i < dis.size(); i++){
		if(min_dis > dis[i]){
			min_dis = dis[i];
			min_dis_index = i;
		}
	}
    return min_dis_index;
}

size_t PlanManage::findTargetIdx(const frenet_trajectory_info::FrenetTrajectory &best_trajectory, const std::array<double, 2> &pos){
	std::vector<double> dis;
    size_t min_dis_index = 0;
	int data_size = best_trajectory.x.size();
	for (size_t i = 0; i < data_size; i++)
		dis.push_back(pow((best_trajectory.x[i] - pos[0]),2) + pow((best_trajectory.y[i] - pos[1]),2));
	
	double min_dis = dis[min_dis_index];
	for (size_t i = min_dis_index ; i < dis.size(); i++){
		if(min_dis > dis[i]){
			min_dis = dis[i];
			min_dis_index = i;
		}
	}
    return min_dis_index;
}

size_t PlanManage::findTargetIdx(const std::vector<std::vector<double>> &data, const std::array<double, 2> &pos, const size_t last_index){
	std::vector<double> dis;
    size_t min_dis_index = 0;
	int data_size = data.size();
    int serch_start_index,serch_end_index;
    serch_start_index = std::max((int)last_index - 100 , 0);
    serch_end_index = std::min((int)last_index + 100 , data_size);
	for (int i = serch_start_index; i < serch_end_index; i++)
		dis.push_back(pow((data[i][0] - pos[0]),2) + pow((data[i][1] - pos[1]),2));

	double min_dis = dis[min_dis_index];
	for (int i = min_dis_index ; i < dis.size(); i++){
		if(min_dis > dis[i]){
			min_dis = dis[i];
			min_dis_index = i;
		}
	}
    min_dis_index = std::min(min_dis_index + serch_start_index, size_t(data_size)-1);
    return min_dis_index;
}

void PlanManage::comput_init_frenet_state(const double x, const double y, 
                                          double* ptr_s, double* ptr_d){
    std::array<double, 2> pos = {x,y};
    size_t index = findTargetIdx(ref_path_,pos);
    double rs = ref_path_[index][2];
    double rx = ref_path_[index][0];
    double ry = ref_path_[index][1];
    double rtheta = ref_path_spline_.calc_yaw(rs);
    common::math::CartesianFrenetConverter::cartesian_to_frenet(rs,rx,ry,rtheta,x,y,ptr_s,ptr_d);                              
}

void PlanManage::comput_frenet_state(const double &x, const double &y,
                                     const frenet_trajectory_info::FrenetTrajectory &best_trajectory,
                                     std::array<double, 3> &S0, std::array<double, 3> &L0){
    std::array<double, 2> pos = {x,y};
    size_t index = findTargetIdx(best_trajectory,pos);
    index = 1;
    S0[0] = best_trajectory.s[index];
    S0[1] = best_trajectory.ds[index];
    S0[2] = best_trajectory.dds[index];
    L0[0] = 0.0;
    L0[1] = 0.0;
    L0[2] = 0.0;     


    // L0[0] = best_trajectory.dl[index];
    // L0[1] = best_trajectory.dl[index];
    // L0[2] = best_trajectory.ddl[index];                             
}

void PlanManage::carOdom_cb(const nav_msgs::Odometry::ConstPtr &odomMsg){
    car_odom_.pose = odomMsg->pose;
    car_odom_.twist = odomMsg->twist;
    if( !flag_receive_odom_) flag_receive_odom_ = true;
}

void PlanManage::changeExecState(EXEC_STATE new_state){
    std::string state_str[] = { "INIT", "WAIT_TARGET", "UPDATE_STATE", "CALC_TRAJ"};
    int  pre_s        = int(exec_state_);
    exec_state_         = new_state;
    std::cout << "ExecState : from " + state_str[pre_s] + " to " + state_str[int(new_state)] << std::endl;
}

void PlanManage::printExecState(){
    std::string state_str[5] = { "INIT", "WAIT_TARGET", "UPDATE_STATE", "CALC_TRAJ"};
    std::cout << "ExecState : " + state_str[int(exec_state_)] << std::endl;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "planer_manage_node");

    ros::NodeHandle nh("");
    ros::NodeHandle nh_private("~");

    PlanManage *plan_manage = new PlanManage(nh, nh_private);
    plan_manage->init();

    ros::spin();

    delete plan_manage;

    return 0;
}