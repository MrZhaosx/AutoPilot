#include "stanley_controller.h"

StanleyController::StanleyController(double gain_k, double gain_d, double kp,double steer_max, double speed_max)
    : gain_k_(gain_k),
      gain_d_(gain_d),
      kp_(kp),
      steer_max_(steer_max),
      speed_max_(speed_max){
}

StanleyController::~StanleyController(){
}

/**
 * @brief 计算参考路径距离车辆当前位置最近点的索引
 * 
 * @param data : 参考路径
 * 
 * @param pos  : 车辆位置
 */
void StanleyController::findTargetIdx(std::vector<std::array<double,2>> &data, std::vector<double> &pos){

	std::vector<double> dis;
	int data_size = data.size();
	for (size_t i = 0; i < data_size; i++)
		dis.push_back(pow((data[i][0] - pos[0]),2) + pow((data[i][1] - pos[1]),2));
	
	min_dis_index_ = 0; 
	min_dis_ = dis[min_dis_index_];
	for (size_t i = min_dis_index_ ; i < dis.size(); i++){
		if(min_dis_ > dis[i]){
			min_dis_ = dis[i];
			min_dis_index_ = i;
		}
	}
}

void StanleyController::findTargetIdx(std::vector<std::array<double,2>> &data, std::vector<double> &pos, size_t last_index){
	std::vector<double> dis;
	int data_size = data.size();
    int serch_start_index,serch_end_index;
    serch_start_index = std::max((int)last_index - 100 , 0);
    serch_end_index = std::min((int)last_index + 100 , data_size);
	for (int i = serch_start_index; i < serch_end_index; i++)
		dis.push_back(pow((data[i][0] - pos[0]),2) + pow((data[i][1] - pos[1]),2));

	min_dis_index_ = 0; 
	min_dis_ = dis[min_dis_index_];
	for (int i = min_dis_index_ ; i < dis.size(); i++){
		if(min_dis_ > dis[i]){
			min_dis_ = dis[i];
			min_dis_index_ = i;
		}
	}
    min_dis_index_ = std::min(min_dis_index_ + serch_start_index, size_t(data_size)-1);
}


void StanleyController::findTargetIdx(std::vector<std::array<double,4>> &data, std::vector<double> &pos){
	std::vector<double> dis;
	int data_size = data.size();
	for (size_t i = 0; i < data_size; i++)
		dis.push_back(pow((data[i][0] - pos[0]),2) + pow((data[i][1] - pos[1]),2));
	
	min_dis_index_ = 0; 
	min_dis_ = dis[min_dis_index_];
	for (size_t i = min_dis_index_ ; i < dis.size(); i++){
		if(min_dis_ > dis[i]){
			min_dis_ = dis[i];
			min_dis_index_ = i;
		}
	}
  double cos_theta_r = std::cos(data[min_dis_index_][2]);
  double sin_theta_r = std::sin(data[min_dis_index_][2]);
  double cross_rd_nd = cos_theta_r * (pos[1] - data[min_dis_index_][1]) - sin_theta_r * (pos[0] - data[min_dis_index_][0]);
  min_dis_ = std::copysign(min_dis_, cross_rd_nd);
}

/**
  * @brief  计算车辆的控制指令
  * @param  ref_path: 参考路径. 
  * @param  current_state:
  *   车辆当前的状态:
  *     @arg current_state[0]: x
  *     @arg current_state[1]: y
  *     @arg current_state[2]: yaw
  *     @arg current_state[3]: v
  * @retval out
  *     @arg out[0]: out_steer
  *     @arg out[1]: out_v
  */
void StanleyController::ComputeControlCommand(const std::vector<std::array<double,2>> &ref_path, 
                                const std::vector<double> &current_state,
                                std::vector<double> &out,
                                const double &target_speed){    
    std::array<double,2> forward_point;   // min_dis_index_ 向前5个点用于计算斜率
    size_t ref_path_size = ref_path.size();
    
    forward_point = ref_path[std::min(min_dis_index_ + 5, ref_path_size-1)];

    theta_fai = atan2((forward_point[1] - current_state[1]), (forward_point[0] - current_state[0])) - current_state[2];
    if (theta_fai > M_PI)
        theta_fai = theta_fai - 2*M_PI;
    else if (theta_fai < -M_PI)
        theta_fai = theta_fai + 2*M_PI;

    theta_y = atan2(gain_k_ * min_dis_ , current_state[3]);

    double out_steer = theta_fai + 0;
    limit_fun(out_steer, steer_max_);

    double out_v = kp_ * (target_speed - current_state[3]);
    limit_fun(out_v, speed_max_);

    out.clear();
    out.push_back(out_steer);
    out.push_back(out_v);

}


void StanleyController::ComputeControlCommand(const std::vector<std::array<double,4>> &ref_path, 
                                              const std::vector<double> &current_state,
                                              std::vector<double> &out){

    std::array<double,4> forward_point;   // min_dis_index_ 向前5个点用于计算斜率
    size_t ref_path_size = ref_path.size();
    
    // forward_point = ref_path[std::min(min_dis_index_ + 5, ref_path_size-1)];

    // theta_fai = atan2((forward_point[1] - current_state[1]), (forward_point[0] - current_state[0])) - current_state[2];
    
    theta_fai = ref_path[min_dis_index_][2] - current_state[2];
    if (theta_fai > M_PI)
        theta_fai = theta_fai - 2*M_PI;
    else if (theta_fai < -M_PI)
        theta_fai = theta_fai + 2*M_PI;


    double err = 0 - min_dis_;
    double dis = gain_k_ * err  + gain_d_ * (err - min_dis_last_);
    min_dis_last_ = err;
    if(current_state[3] > 0.5)
        theta_y = atan2(dis , current_state[3]);
    else
        theta_y = 0;
        
    std::cout << "theta_y " << theta_y*180/M_PI << std::endl;
    std::cout << "theta_fai " << theta_fai*180/M_PI << std::endl;
    double out_steer = theta_fai + theta_y;
    limit_fun(out_steer, steer_max_);

    double out_v = kp_ * (ref_path[min_dis_index_][3] - current_state[3]);
    limit_fun(out_v, speed_max_);

    out.clear();
    out.push_back(out_steer);
    out.push_back(out_v);                                                
}

/**
 * @brief 限幅函数
 * 
 * @param data      待限幅的变量
 * @param amplitude 幅值
 */
void StanleyController::limit_fun(double &data, const double amplitude){
    if(data > std::abs(amplitude))
        data = std::abs(amplitude);
    else if(data < -std::abs(amplitude))
        data = -std::abs(amplitude);
}

/**
 * @brief get min_dis_index_
 * 
 * @param 无
 * @retval min_dis_index_
 */
size_t StanleyController::getMinDisIndex(){
    return min_dis_index_;
}