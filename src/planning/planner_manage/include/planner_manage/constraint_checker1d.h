#ifndef constraint_checker1d_h
#define constraint_checker1d_h

#include "polynomial_calculation.h"
#include "common_config.h"

namespace lp {

class ConstraintChecker1d {

 public:
  ConstraintChecker1d() = delete;

  static bool IsValidLongitudinalTrajectory(const polynomial::PolynomialCurve1d& lon_trajectory);

  static bool IsValidLateralTrajectory(const polynomial::PolynomialCurve1d& lat_trajectory,
                                       const polynomial::PolynomialCurve1d& lon_trajectory);
};

}  // namespace planning

#endif