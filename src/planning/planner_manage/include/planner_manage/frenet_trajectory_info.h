#ifndef FRENET_TRAJECTORY_INFO_H
#define FRENET_TRAJECTORY_INFO_H

#include <vector>


namespace frenet_trajectory_info
{

class FrenetTrajectory
{
public:

    // Longitudinal coordinate
    std::vector<double> s;    // Longitudinal position
    std::vector<double> ds;   // Longitudinal velocity
    std::vector<double> dds;  // Longitudinal acceleration
    std::vector<double> ddds; // Longitudinal jerk
        
    // Lateral coordinate
    std::vector<double> l;    // Lateral position
    std::vector<double> dl;   // Lateral velocity
    std::vector<double> ddl;  // Lateral acceleration
    std::vector<double> dddl; // Lateral jerk

    std::vector<double> l_dot;      // dl/ds
    std::vector<double> l_ddot;     // d(dl/ds)ds
    // std::vector<double> l_dddot;    // d(d(dl/ds)ds)/ds

    // Costs
    double Jl; // Lateral cost
    double Js; // Longitudinal cost
    double J;  // Total cost
        
    // World coordinates
    std::vector<double> x;     // x position
    std::vector<double> y;     // y position
    std::vector<double> theta; // Orientation
    std::vector<double> kappa; // Curvature
    std::vector<double> dL;    // Running length / arc length
    std::vector<double> v;     // Tangential velocity
    std::vector<double> a;     // Tangential acceleration


    FrenetTrajectory(/* args */);
    ~FrenetTrajectory();
    void clear();
};

} // namespace frenet_trajectory_info


#endif