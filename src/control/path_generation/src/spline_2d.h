#ifndef SPLINE_2D_H
#define SPLINE_2D_H

#include <iostream>
#include <vector>
#include <math.h>
#include "spline.h"


class Spline2D
{
private:
    /* data */
    std::vector<double> s_;

    tk::spline spline_x_, spline_y_;
    void calc_s(std::vector<double> &x, std::vector<double> &y, std::vector<double> &s);

public:
    Spline2D(std::vector<double> &X, std::vector<double> &Y);
    ~Spline2D();

    double calc_k(double s);
    double calc_yaw(double s);
    std::vector<double> calc_pos(double s);
};




#endif