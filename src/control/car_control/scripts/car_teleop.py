#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from geometry_msgs.msg import Twist
import sys, select, termios, tty
from prius_msgs.msg import Control

#  throttle             --  w
#  brake                --  s
#  steer                --  a d
#  NEUTRAL=1 #空挡       --  u
#  FORWARD=2 #前进       --  i
#  REVERSE=3 #倒车       --  o

msg = """
Control car!
---------------------------
#  throttle         --  w
#  brake            --  s
#  steer            --  a d
#  NEUTRAL=1        --  u
#  FORWARD=2        --  i
#  REVERSE=3        --  o

CTRL-C to quit
"""

moveBindings = {
        'i':(1,0),
        'o':(1,-1),
        'j':(0,1),
        'l':(0,-1),
        'u':(1,1),
        ',':(-1,0),
        '.':(-1,1),
        'm':(-1,-1),
           }

speedBindings={
        'q':(1.1,1.1),
        'z':(.9,.9),
        'w':(1.1,1),
        'x':(.9,1),
        'e':(1,1.1),
        'c':(1,.9),
          }

def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

speed = .2
turn = 1

def vels(speed,turn):
    return "currently:\tspeed %s\tturn %s " % (speed,turn)

if __name__=="__main__":
    settings = termios.tcgetattr(sys.stdin)
    
    rospy.init_node('car_teleop')
    pub = rospy.Publisher('/prius', Control, queue_size=5)

    x = 0
    th = 0
    status = 0
    count = 0
    acc = 0.1
    target_speed = 0
    target_turn = 0
    control_speed = 0
    control_turn = 0
    try:
        print msg
        command = Control()
        while(1):
            key = getKey()
            # 运动控制方向键（1：正方向，-1负方向）
            if key == 'w':
                command.throttle = 0.15
                command.brake = 0.0
                count = 0
            elif key == 's':
                command.brake = 0.2
                command.throttle = 0.0
                count = 0
            elif key == 'a':
                 command.steer = 0.35
                 count = 0
            elif key == 'd':
                 command.steer = -0.35
                 count = 0

            # 切换档位
            elif key == 'u':
                 command.shift_gears = Control.FORWARD
                 count = 0
            elif key == 'i':
                 command.shift_gears = Control.NEUTRAL
                 count = 0
            elif key == 'o':
                 command.shift_gears = Control.REVERSE
                 count = 0
            else:
                count = count + 1
                if count > 4:
                    command.steer = 0.0
                    command.brake = 0.0
                    command.throttle = 0.0
                if (key == '\x03'):
                    break


            command.header.stamp = rospy.Time.now()
            pub.publish(command)

    except:
        print e

    finally:
        command.shift_gears = Control.NEUTRAL
        command.steer = 0.0
        command.brake = 1.0
        command.throttle = 0.0
        pub.publish(command)

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
