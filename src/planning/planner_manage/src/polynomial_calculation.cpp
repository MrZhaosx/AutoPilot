#include "planner_manage/polynomial_calculation.h"


namespace polynomial{

QuinticPoly::QuinticPoly(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T){
    p_.resize(6);
    set_parameters(x0, xt, T);
}

QuinticPoly::QuinticPoly(){
    p_.resize(6);

}

QuinticPoly::~QuinticPoly()
{
}

void QuinticPoly::set_parameters(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T){
    Eigen::Vector3d c_012;
    Eigen::Vector3d xt_vec;
    Eigen::Matrix3d M1;
    Eigen::Matrix3d M2;
    Eigen::Vector3d b;
    Eigen::Vector3d c_345;

    max_x_ = std::max(x0[0], xt[0]);
    min_x_ = std::min(x0[0], xt[0]);
    param_ = T;

    c_012 << x0[0], x0[1], x0[2]/2.0;
    xt_vec << xt[0], xt[1], xt[2];
    
    M1 << 1.0,  T,  T*T,
          0.0,  1.0,  2*T,
          0.0,  0.0,  2.0;
    M2 << T*T*T,  T*T*T*T,  T*T*T*T*T,
          3*T*T,  4*T*T*T,  5*T*T*T*T,
          6*T,    12*T*T,    20*T*T*T;
    
    b = xt_vec - M1 * c_012;

    c_345 = M2.lu().solve(b);
    // c_345 = M2.inverse()*b;

    p_ << c_012, c_345;
}

double QuinticPoly::get_y(double x) const {
    return p_(0) + p_(1)*x + p_(2) * std::pow(x,2) + p_(3) * std::pow(x,3) + p_(4) * std::pow(x,4) + p_(5) * std::pow(x,5);
}

double QuinticPoly::get_deriv(int order, double x) const {
    double interpol;

    // assert(order>0);
    // assert(x>max_x_);
    // assert(x<min_x_);
    
    switch (order)
    {
    case 0:
        interpol = p_(0) + p_(1)*x + p_(2) * std::pow(x,2) + p_(3) * std::pow(x,3) + p_(4) * std::pow(x,4);
        break;
    case 1:
        interpol = p_(1) + 2 * p_(2) * x + 3 * p_(3) * std::pow(x,2) + 4 * p_(4) * std::pow(x,3) + 5 * p_(5) * std::pow(x,4);
        break;
    case 2:
        interpol = 2 * p_(2) + 6 * p_(3) * x + 12 * p_(4) * std::pow(x,2) + 20 * p_(5) * std::pow(x,3);
        break;
    case 3:
        interpol = 2 + 6 * p_(3) + 24 * p_(4) * x + 60 * p_(5) * std::pow(x,2);
        break;
    default:
        interpol = 0.0;
        break;
    }
    return interpol;
}


// **************************** 四次多项式 ***********************************

QuarticPoly::QuarticPoly(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T){
    p_.resize(5);
    set_parameters(x0, xt, T);
}

QuarticPoly::QuarticPoly(){
    p_.resize(5);
}

QuarticPoly::~QuarticPoly()
{
}

void QuarticPoly::set_parameters(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T){
    Eigen::Vector3d c_012;
    Eigen::Vector2d xt_vec;
    Eigen::Matrix<double,2,3> M1;
    Eigen::Matrix<double,2,2> M2;
    Eigen::Vector2d b;
    Eigen::Vector2d c_345;

    
    max_x_ = std::max(x0[0], xt[0]);
    min_x_ = std::min(x0[0], xt[0]);
    param_ = T;

    c_012 << x0[0], x0[1], x0[2]/2.0;
    xt_vec << xt[1], xt[2];             // 末端位置不需要
    
    M1 << 0.0,  1.0,  2*T,
          0.0,  0.0,  2.0;
    M2 << 3*T*T,  4*T*T*T, 
          6*T,    12*T*T;
    
    b = xt_vec - M1 * c_012;

    c_345 = M2.lu().solve(b);

    p_ << c_012, c_345;
    // std::cout << p_[0] << "  " << p_[1] << "  " << p_[2] << "  " << p_[3] << "  " << p_[4] << std::endl;
    
}

double QuarticPoly::get_y(double x) const {
    return p_(0) + p_(1)*x + p_(2) * std::pow(x,2) + p_(3) * std::pow(x,3) + p_(4) * std::pow(x,4);
}

double QuarticPoly::get_deriv(int order, double x) const {
    double interpol;

    // assert(order>0);
    // assert(x>max_x_);
    // assert(x<min_x_);
    // std::cout << p_[0] << "  " << p_[1] << "  " << p_[2] << "  " << p_[3] << "  " << p_[4] << std::endl;
    switch (order)
    {
    case 0:
        interpol = p_(0) + p_(1)*x + p_(2) * std::pow(x,2) + p_(3) * std::pow(x,3) + p_(4) * std::pow(x,4);
        break;
    case 1:
        interpol = p_(1) + 2 * p_(2) * x + 3 * p_(3) * std::pow(x,2) + 4 * p_(4) * std::pow(x,3);
        break;
    case 2:
        interpol = 2 * p_(2) + 6 * p_(3) * x + 12 * p_(4) * std::pow(x,2);
        break;
    case 3:
        interpol = 2 + 6 * p_(3) + 24 * p_(4) * x;
        break;
    default:
        interpol = 0.0;
        break;
    }
    return interpol;
}

}

/* polynomial 测试  */
// int main(int, char **)
// {
//     std::array<double, 3>x0 = {0.0, 2.77780, 0};
//     std::array<double, 3>xt = {0.0, 6.9440, 0};
//     polynomial::QuarticPoly quartic_poly(x0,xt,4.2);
//     std::cout << "hello " << std::endl;
//     return 0;
// }