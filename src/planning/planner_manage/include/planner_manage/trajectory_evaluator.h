#ifndef trajectory_evaluator_h
#define trajectory_evaluator_h

#include <iostream>
#include <array>
#include <vector>
#include <memory>
#include <queue>
#include "spline_2d.h"
#include "spline.h"
#include "polynomial_calculation.h"
#include "common_config.h"
#include "constraint_checker1d.h"
#include "planner_manage/PiecewiseAccelerationTrajectory1d.h"

namespace lp
{

using Curve1d = polynomial::PolynomialCurve1d ;
using Trajectory1d = polynomial::PolynomialCurve1d;
using PtrTrajectory1d = std::shared_ptr<Trajectory1d>;

// typedef std::vector<std::shared_ptr<polynomial::PolynomialCurve1d>> Trajectory1DBundle;


using State = std::array<double, 3>;
using Condition = std::pair<State, double>;

using Trajectory1dPair = std::pair< std::shared_ptr<Curve1d>, std::shared_ptr<Curve1d> >;

  // normal use
typedef std::pair<
        std::pair<std::shared_ptr<polynomial::PolynomialCurve1d>, std::shared_ptr<polynomial::PolynomialCurve1d>>, double>
        PairCost;


class TrajectoryEvaluator
{
private:
    /* data */
    struct CostComparator
        : public std::binary_function<const PairCost&, const PairCost&, bool> {
        bool operator()(const PairCost& left, const PairCost& right) const {
        return left.second > right.second;
        }
    };

    std::array<double, 3> init_s_;
    std::priority_queue<PairCost, std::vector<PairCost>, CostComparator> cost_queue_;
    std::vector<double> reference_s_dot_;
    spline_2d::Spline2D* ptr_reference_line_;

    double Evaluate(const double& planning_target,
                    const std::shared_ptr<polynomial::PolynomialCurve1d>& lon_trajectory,
                    const std::shared_ptr<polynomial::PolynomialCurve1d>& lat_trajectory,
                    std::vector<double>* cost_components = nullptr) const;

	double LatOffsetCost(const std::shared_ptr<Curve1d>& lat_trajectory,
						const std::vector<double>& s_values) const;

	double LatComfortCost(const std::shared_ptr<Curve1d>& lon_trajectory,
							const std::shared_ptr<Curve1d>& lat_trajectory) const;

	double LonComfortCost(const std::shared_ptr<Curve1d>& lon_trajectory) const;

	double LonCollisionCost(const std::shared_ptr<Curve1d>& lon_trajectory) const;

	double LonObjectiveCost(const std::shared_ptr<Curve1d>& lon_trajectory,
							const double& planning_target,
							const std::vector<double>& ref_s_dot) const;

	double CentripetalAccelerationCost(const std::shared_ptr<Curve1d>& lon_trajectory) const;


  std::vector<double> ComputeLongitudinalGuideVelocity(const double& planning_target);

public:
    TrajectoryEvaluator(
      const std::array<double, 3>& init_s,
      const double& planning_target,
      const std::vector<std::shared_ptr<polynomial::PolynomialCurve1d>>& lon_trajectories,
      const std::vector<std::shared_ptr<polynomial::PolynomialCurve1d>>& lat_trajectories,
      spline_2d::Spline2D &reference_line);
    ~TrajectoryEvaluator() = default;

  bool has_more_trajectory_pairs() const;

  size_t num_of_trajectory_pairs() const;

  double top_trajectory_pair_cost() const;

  std::pair<std::shared_ptr<Curve1d>, std::shared_ptr<Curve1d>> next_top_trajectory_pair();
};



} // namespace lp
#endif