#include <iostream>
#include <ros/ros.h> 
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

nav_msgs::Odometry groundTruth;
nav_msgs::Odometry carOdom;
geometry_msgs::TransformStamped odom_trans;


void groundTruth_cb(const nav_msgs::Odometry::ConstPtr& msg)
{
    groundTruth.pose = msg->pose;
    groundTruth.twist = msg->twist;
}

int main(int argc, char *argv[])
{
    
    ros::init(argc, argv, "odom_transform_node");
    ros::NodeHandle nh;//创建句柄
        
    ros::Subscriber groundTruth_sub;
    ros::Publisher odom_pub;
	ros::Time current_time;
	tf::TransformBroadcaster odom_broadcaster;

    groundTruth_sub = nh.subscribe<nav_msgs::Odometry>("/base_pose_ground_truth", 1, groundTruth_cb);
    odom_pub = nh.advertise<nav_msgs::Odometry>("/prius/odom", 1);

    ros::Rate loop_rate(50);

    while(ros::ok()) 
    {
        current_time = ros::Time::now();

        odom_trans.header.stamp = current_time;
        odom_trans.header.frame_id = "map";
        odom_trans.child_frame_id = "base_link";
        odom_trans.transform.translation.x = groundTruth.pose.pose.position.x;
        odom_trans.transform.translation.y = groundTruth.pose.pose.position.y;
        odom_trans.transform.translation.z = groundTruth.pose.pose.position.z;
        odom_trans.transform.rotation = groundTruth.pose.pose.orientation;
		//send the transform
    	odom_broadcaster.sendTransform(odom_trans);


        carOdom.header.stamp = current_time;     
        carOdom.header.frame_id = "map";
        carOdom.child_frame_id = "base_link";
        carOdom.pose = groundTruth.pose;
        carOdom.twist = groundTruth.twist;
        odom_pub.publish(carOdom);

        ros::spinOnce();
        loop_rate.sleep();
    }    

}
