#include "planner_manage/frenet_trajectory_info.h"

namespace frenet_trajectory_info
{

FrenetTrajectory::FrenetTrajectory(/* args */)
{
}

FrenetTrajectory::~FrenetTrajectory()
{
}

void FrenetTrajectory::clear(){

    s.clear();
    ds.clear();
    dds.clear();
    ddds.clear();

    l.clear();
    dl.clear();
    ddl.clear();
    dddl.clear();

    l_dot.clear();
    l_ddot.clear();

    x.clear();
    y.clear();
    theta.clear();
    kappa.clear();
    dL.clear();
    v.clear();
    a.clear();

}

}