#include "planner_manage/trajectory_evaluator.h"

namespace lp
{

TrajectoryEvaluator::TrajectoryEvaluator(
				const std::array<double, 3> &init_s,
				const double &planning_target,
				const std::vector<std::shared_ptr<polynomial::PolynomialCurve1d>>& lon_trajectories,
				const std::vector<std::shared_ptr<polynomial::PolynomialCurve1d>>& lat_trajectories,
				spline_2d::Spline2D &reference_line)
				:init_s_(init_s),
				ptr_reference_line_(&reference_line){//
		
		const double start_time = 0.0;
		const double end_time = FLAGS_trajectory_time_length;

		reference_s_dot_ = ComputeLongitudinalGuideVelocity(planning_target);

		for (const auto& lon_trajectory : lon_trajectories) {

				double lon_end_s = lon_trajectory->get_deriv(0, end_time);

				if ( !ConstraintChecker1d::IsValidLongitudinalTrajectory(*lon_trajectory))
					continue;

				for (const auto& lat_trajectory : lat_trajectories) {
				/**
				 * The validity of the code needs to be verified.
				 if (!ConstraintChecker1d::IsValidLateralTrajectory(*lat_trajectory,
																														*lon_trajectory)) {
						continue;
				}
				*/
				double cost = Evaluate(planning_target, lon_trajectory, lat_trajectory);
				cost_queue_.emplace(Trajectory1dPair(lon_trajectory, lat_trajectory),
														cost);
				}
		}
}

// TrajectoryEvaluator::~TrajectoryEvaluator(){
// }

double TrajectoryEvaluator::Evaluate(const double& planning_target,
										const std::shared_ptr<polynomial::PolynomialCurve1d>& lon_trajectory,
										const std::shared_ptr<polynomial::PolynomialCurve1d>& lat_trajectory,
										std::vector<double>* cost_components) const {
	// Costs:
	// 1. Cost of missing the objective, e.g., cruise, stop, etc.
	// 2. Cost of longitudinal jerk
	// 3. Cost of longitudinal collision
	// 4. Cost of lateral offsets
	// 5. Cost of lateral comfort

	// Longitudinal costs
	double lon_objective_cost = LonObjectiveCost(lon_trajectory, planning_target, reference_s_dot_);

	double lon_jerk_cost = LonComfortCost(lon_trajectory);

//   double lon_collision_cost = LonCollisionCost(lon_trajectory);

	double centripetal_acc_cost = CentripetalAccelerationCost(lon_trajectory);

	// decides the longitudinal evaluation horizon for lateral trajectories.
	// 确定横向轨迹的纵向评估范围
	double evaluation_horizon =
			std::min(FLAGS_speed_lon_decision_horizon,
							 lon_trajectory->get_deriv(0, lon_trajectory->ParamLength()));
	std::vector<double> s_values;
	for (double s = 0.0; s < evaluation_horizon;
			 s += FLAGS_trajectory_space_resolution) {
		s_values.emplace_back(s);
	}

	// Lateral costs
	double lat_offset_cost = LatOffsetCost(lat_trajectory, s_values);

	double lat_comfort_cost = LatComfortCost(lon_trajectory, lat_trajectory);

	if (cost_components != nullptr) {
		cost_components->emplace_back(lon_objective_cost);
		cost_components->emplace_back(lon_jerk_cost);
		// cost_components->emplace_back(lon_collision_cost);
		cost_components->emplace_back(lat_offset_cost);
	}

	return lon_objective_cost * FLAGS_weight_lon_objective +
				 lon_jerk_cost * FLAGS_weight_lon_jerk +
				//  lon_collision_cost * FLAGS_weight_lon_collision +
				 centripetal_acc_cost * FLAGS_weight_centripetal_acceleration +
				 lat_offset_cost * FLAGS_weight_lat_offset +
				 lat_comfort_cost * FLAGS_weight_lat_comfort;
}

double TrajectoryEvaluator::LonObjectiveCost(const PtrTrajectory1d& lon_trajectory,
											const double& planning_target,
											const std::vector<double>& ref_s_dots) const {
		double t_max = lon_trajectory->ParamLength();
		double dist_s = lon_trajectory->get_deriv(0, t_max) - lon_trajectory->get_deriv(0, 0.0);

		double speed_cost_sqr_sum = 0.0;
		double speed_cost_weight_sum = 0.0;
		for (size_t i = 0; i < ref_s_dots.size(); ++i) {
				double t = static_cast<double>(i) * FLAGS_trajectory_time_resolution;
				double cost = ref_s_dots[i] - lon_trajectory->get_deriv(1, t);
				speed_cost_sqr_sum += t * t * std::fabs(cost);
				speed_cost_weight_sum += t * t;
		}
		double speed_cost =
				speed_cost_sqr_sum / (speed_cost_weight_sum + FLAGS_numerical_epsilon);
		double dist_travelled_cost = 1.0 / (1.0 + dist_s);
		return (speed_cost * FLAGS_weight_target_speed +
						dist_travelled_cost * FLAGS_weight_dist_travelled) /
						(FLAGS_weight_target_speed + FLAGS_weight_dist_travelled);
}

double TrajectoryEvaluator::LonComfortCost(const PtrTrajectory1d& lon_trajectory) const {
	double cost_sqr_sum = 0.0;
	double cost_abs_sum = 0.0;
	for (double t = 0.0; t < FLAGS_trajectory_time_length;
			 t += FLAGS_trajectory_time_resolution) {
		double jerk = lon_trajectory->get_deriv(3, t);
		double cost = jerk / FLAGS_longitudinal_jerk_upper_bound;
		cost_sqr_sum += cost * cost;
		cost_abs_sum += std::fabs(cost);
	}
	return cost_sqr_sum / (cost_abs_sum + FLAGS_numerical_epsilon);
}

double TrajectoryEvaluator::CentripetalAccelerationCost(const PtrTrajectory1d& lon_trajectory) const {
	// Assumes the vehicle is not obviously deviate from the reference line.
	double centripetal_acc_sum = 0.0;
	double centripetal_acc_sqr_sum = 0.0;
	for (double t = 0.0; t < FLAGS_trajectory_time_length;
			 t += FLAGS_trajectory_time_resolution) {
		double s = lon_trajectory->get_deriv(0, t);
		double v = lon_trajectory->get_deriv(1, t);
		// PathPoint ref_point = PathMatcher::MatchToPath(*reference_line_, s);
		// ACHECK(ref_point.has_kappa());
		double kappa = ptr_reference_line_->calc_k(s);
		double centripetal_acc = v * v * kappa;
		centripetal_acc_sum += std::fabs(centripetal_acc);
		centripetal_acc_sqr_sum += centripetal_acc * centripetal_acc;
	}

	return centripetal_acc_sqr_sum /
				 (centripetal_acc_sum + FLAGS_numerical_epsilon);
}

double TrajectoryEvaluator::LatOffsetCost(
		const PtrTrajectory1d& lat_trajectory,
		const std::vector<double>& s_values) const {
	double lat_offset_start = lat_trajectory->get_deriv(0, 0.0);
	double cost_sqr_sum = 0.0;
	double cost_abs_sum = 0.0;
	for (const auto& s : s_values) {
		double lat_offset = lat_trajectory->get_deriv(0, s);
		double cost = lat_offset / FLAGS_lat_offset_bound;
		if (lat_offset * lat_offset_start < 0.0) {
			cost_sqr_sum += cost * cost * FLAGS_weight_opposite_side_offset;
			cost_abs_sum += std::fabs(cost) * FLAGS_weight_opposite_side_offset;
		} else {
			cost_sqr_sum += cost * cost * FLAGS_weight_same_side_offset;
			cost_abs_sum += std::fabs(cost) * FLAGS_weight_same_side_offset;
		}
	}
	return cost_sqr_sum / (cost_abs_sum + FLAGS_numerical_epsilon);
}

double TrajectoryEvaluator::LatComfortCost(
		const PtrTrajectory1d& lon_trajectory,
		const PtrTrajectory1d& lat_trajectory) const {
	double max_cost = 0.0;
	for (double t = 0.0; t < FLAGS_trajectory_time_length;
			 t += FLAGS_trajectory_time_resolution) {
		double s = lon_trajectory->get_deriv(0, t);
		double s_dot = lon_trajectory->get_deriv(1, t);
		double s_dotdot = lon_trajectory->get_deriv(2, t);

		double relative_s = s - init_s_[0];
		double l_prime = lat_trajectory->get_deriv(1, relative_s);
		double l_primeprime = lat_trajectory->get_deriv(2, relative_s);
		double cost = l_primeprime * s_dot * s_dot + l_prime * s_dotdot;
		max_cost = std::max(max_cost, std::fabs(cost));
	}
	return max_cost;
}

std::vector<double> TrajectoryEvaluator::ComputeLongitudinalGuideVelocity(const double &planning_target){
		std::vector<double> reference_s_dot;
		double cruise_v = planning_target;

		PiecewiseAccelerationTrajectory1d lon_traj(init_s_[0], cruise_v);
		lon_traj.AppendSegment(0.0, FLAGS_trajectory_time_length + FLAGS_numerical_epsilon);

		for (double t = 0.0; t < FLAGS_trajectory_time_length;
				 t += FLAGS_trajectory_time_resolution) {
			reference_s_dot.emplace_back(lon_traj.get_deriv(1, t));
		}

		return reference_s_dot;
}

bool TrajectoryEvaluator::has_more_trajectory_pairs() const {
	return !cost_queue_.empty();
}

size_t TrajectoryEvaluator::num_of_trajectory_pairs() const {
	return cost_queue_.size();
}

double TrajectoryEvaluator::top_trajectory_pair_cost() const{
	return cost_queue_.top().second;
}

std::pair<std::shared_ptr<Curve1d>, std::shared_ptr<Curve1d>> TrajectoryEvaluator::next_top_trajectory_pair(){
	
	if( !has_more_trajectory_pairs() ) { COUT_ERROR; return {}; }

	auto top = cost_queue_.top();
	cost_queue_.pop();
	return top.first;
}

} // namespace lp
