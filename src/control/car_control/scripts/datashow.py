#!/usr/bin/env python

from matplotlib import pyplot as plt
import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray

class DataShow:

    def __init__(self):

        rospy.Subscriber("/base_pose_ground_truth", Odometry, self.odom_cb)
        rospy.Subscriber("/show/multi/array", Float32MultiArray, self.show_multi_array_cb)
        self.rate = rospy.Rate(10) # 10hz

        # *************** global variable ***************
        self.odom_x_now = 1.0
        self.odom_y_now = 1.0
        self.odom_x_last = 0.5
        self.odom_y_last = 0.5

        self.show_multi_array = []
        self.flag_show_multi_array = False

        self.show_multi_array_x_axis = 0
        self.cureent_speed = 0
        self.cmd_throttle = 0
        self.cmd_brake = 0

        plt.ion()
        self.fig = plt.figure(num=1)
        self.ax1 = self.fig.add_subplot(111)
        self.fig = plt.figure(num=2)
        self.ax21 = self.fig.add_subplot(211)
        self.ax22 = self.fig.add_subplot(212)
        
    # *************** callback function ***************
    def odom_cb(self,data):
        self.odom_x_now = data.pose.pose.position.x
        self.odom_y_now = data.pose.pose.position.y

    # data:
    #   0 -- current_state_.yaw
    #   1 -- cmd_steer
    #   2 -- theta_fai
    #   3 -- theta_y
    #   4 -- current_state_.v
    #   5 -- target_speed_
    #   6 -- control_cmd_.throttle
    #   7 -- control_cmd_.brake

    def show_multi_array_cb(self,data):
        self.show_multi_array = data.data
        self.flag_show_multi_array = True

    def show(self):

        while not rospy.is_shutdown():

            # *************** plot odom ***************
            if (self.odom_x_now-self.odom_x_last)**2 + (self.odom_y_now-self.odom_y_last)**2 > 0.01 :
                self.ax1.plot([self.odom_x_last, self.odom_x_now], [self.odom_y_last, self.odom_y_now],'r') 
                
            self.odom_x_last = self.odom_x_now
            self.odom_y_last = self.odom_y_now
            
            # *************** plot speed ***************
            if self.flag_show_multi_array :
                self.flag_show_multi_array = False
                self.ax21.plot([self.show_multi_array_x_axis, self.show_multi_array_x_axis+1],[self.show_multi_array[5], self.show_multi_array[5]],'r')
                self.ax21.plot([self.show_multi_array_x_axis, self.show_multi_array_x_axis+1],[self.cureent_speed, self.show_multi_array[4]],'g')
                
                self.ax22.plot([self.show_multi_array_x_axis, self.show_multi_array_x_axis+1],[self.cmd_throttle, self.show_multi_array[6]],'g')
                self.ax22.plot([self.show_multi_array_x_axis, self.show_multi_array_x_axis+1],[self.cmd_brake, self.show_multi_array[7]],'r')

                self.show_multi_array_x_axis = self.show_multi_array_x_axis + 1
                self.cureent_speed = self.show_multi_array[4]
                self.cmd_throttle = self.show_multi_array[6]
                self.cmd_brake  = self.show_multi_array[7]

            plt.pause(0.001)
            self.rate.sleep()
        
if __name__ == '__main__':

    rospy.init_node('car_data_show_node')

    data_show = DataShow()
    data_show.show()
