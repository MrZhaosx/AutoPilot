#ifndef pid_controller_h
#define pid_controller_h

#include <iostream>
#include <string>
#include <math.h>
#include <array>
#include "common_msgs/PlannerCommand.h"

namespace controller
{


class PIDController
{
private:
    /* data */

    struct PID
    {
        double kp;
        double ki;
        double kd;
        double err;
        double err_last;
        double err_i;
        double out;
    };
    PID pid_yaw_, pid_velocity_; 

    double steer_max_, velocity_max_;
    void limit_fun(double &data, const double amplitude);

public:
    PIDController(double steer_max, double velocity_max);
    ~PIDController();

/**
  * @brief  计算车辆的控制指令
  * @param  planner_cmd: 规划器指令
  * @param  current_state:
  *   车辆当前的状态:
  *     @arg current_state[0]: x
  *     @arg current_state[1]: y
  *     @arg current_state[2]: yaw
  *     @arg current_state[3]: v
  * @retval out
  *     @arg out[0]: out_steer
  *     @arg out[1]: out_v
  */
    void compute_control_command(const common_msgs::PlannerCommand &planner_cmd, 
                                 const std::array<double,4> &current_state,
                                 std::array<double,2> &out);
};



} // namespace controller








#endif