#ifndef polynomial_calculation_h
#define polynomial_calculation_h

#include <iostream>
#include <vector>
#include <array>
#include <math.h>
#include <cassert>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>

namespace polynomial{

class PolynomialCurve1d
{
protected:
    /* data */

public:
    PolynomialCurve1d() = default;
    ~PolynomialCurve1d() = default; 

    virtual double get_y(double x) const = 0;

    virtual double get_deriv(int order, double x) const = 0;
    
    virtual double ParamLength() const = 0;

    // virtual std::string ToString() const = 0;

};




class QuinticPoly : public PolynomialCurve1d  // 五次多项式
{
private:
    /* data */
    Eigen::VectorXd p_;
    double max_x_,min_x_;
    double param_;
public:
/**
 * @brief Construct a new Quintic Poly object
 * 
 * @param x0 : [x(0) dx(0) ddx(0)]
 * @param xt : [x(t) dx(t) ddx(t)]
 * @param T  : T = t - 0
 */
    QuinticPoly(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T);
    QuinticPoly();
    ~QuinticPoly();

/**
 * @brief Set parameters
 * 
 * @param x0 : [x(0) dx(0) ddx(0)]
 * @param xt : [x(t) dx(t) ddx(t)]
 * @param T  : T = t - 0
 */
    void set_parameters(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T);

    double get_y(double x) const override ;

    double get_deriv(int order, double x) const override ;

    double ParamLength() const override { return param_; }
};


class QuarticPoly : public PolynomialCurve1d  // 四次多项式
{
private:
    /* data */
    Eigen::VectorXd p_;
    double max_x_,min_x_;
    double param_;

public:
    QuarticPoly(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T);
    QuarticPoly();

    ~QuarticPoly();

/**
 * @brief Set parameters
 * 
 * @param x0 : [x(0) dx(0) ddx(0)]
 * @param xt : [x(t) dx(t) ddx(t)]
 * @param T  : T = t - 0
 */
    void set_parameters(const std::array<double, 3> &x0, const std::array<double, 3> &xt, const double T);

    double get_y(double x) const override ;

    double get_deriv(int order, double x) const override;

    double ParamLength() const override { return param_; }

};


} // namespace polynomial






#endif