#ifndef StanleyController_H
#define StanleyController_H
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <array>

#define NDEBUG 1

class StanleyController
{
private:
    /* data */

    double gain_k_;
    double gain_d_;
    double kp_;
    double steer_max_, speed_max_;

    size_t min_dis_index_;
    double min_dis_;
    double min_dis_last_;
    // double out_steer;
    // double out_v;

    void limit_fun(double &data, const double amplitude);

public:
    StanleyController(double gain_k, double gain_d, double kp, double steer_max, double speed_max);
    ~StanleyController();

    void ComputeControlCommand(const std::vector<std::array<double,2>> &ref_path, 
                                const std::vector<double> &current_state,
                                std::vector<double> &out,
                                const double &target_speed);

    void ComputeControlCommand(const std::vector<std::array<double,4>> &ref_path, 
                                const std::vector<double> &current_state,
                                std::vector<double> &out);

/**
 * @brief 计算参考路径距离车辆当前位置最近点的索引
 * 
 * @param data : 参考轨迹
 *              data[i][0] : x
 *              data[i][1] : y
 *              data[i][2] : theat
 *              data[i][3] : v 
 * @param pos  : 车辆位置
 */
    void findTargetIdx(std::vector<std::array<double,4>> &data, std::vector<double> &pos);                                
    void findTargetIdx(std::vector<std::array<double,2>> &data, std::vector<double> &pos);
    void findTargetIdx(std::vector<std::array<double,2>> &data, std::vector<double> &pos, size_t last_index);
    size_t getMinDisIndex();

    double theta_fai,theta_y;

};



#endif