#ifndef LATTICE_PLANNER_H
#define LATTICE_PLANNER_H

#include <iostream>
#include <vector>
#include <list>
#include <float.h>
#include "polynomial_calculation.h"
#include "spline_2d.h"
#include "spline.h"
#include "frenet_trajectory_info.h"
#include "cartesian_frenet_converter.h"
#include "planner_manage/constraint_checker1d.h"
#include "common_config.h"
#include "planner_manage/trajectory_combiner.h"
#include "trajectory_evaluator.h"
#include "planner_manage/constraint_checker.h"
#include "planner_manage/lattice_trajectory1d.h"

#include <memory>
#include <utility>

namespace lp
{

typedef std::vector<std::shared_ptr<polynomial::PolynomialCurve1d>> Trajectory1DBundle;
using State = std::array<double, 3>;
using Condition = std::pair<State, double>;

class LatticePlanner
{
private:

    /* data */


    double MAX_SPEED_ = 20.0/3.6;
    double MAX_ACCEL_ = 2.0;

    double MAX_CURVATURE_ = 1.0;
    double MAX_ROAD_WIDTH_ = 5.0;
    double MIN_ROAD_WIDTH_ = -1.0;
    double D_ROAD_WIDTH_ = 1.0;              // road width sampling length[m]

    double D_T_=0.5;                         // time tick [s]
    double MAX_T_=7.0;                       // max prediction time [s]
    double MIN_T_=4.0;                       // min prediction time[s]

    double TARGET_SPEED_=15.0/3.6;           // target speed [s]
    double D_TARGET_SPEED_=2/3.6;            // target speed sampling length [m/s]
    double N_S_SAMPLE_=2;                    // sampling number of target speed

    double ROBOT_RADIUS_=1.0;                // robot radius [m]

    // cost weight
    double KJ = 0.1;                        // Jerk     
    double KT = 0.1;                        // time
    double KL = 1.0;                        // Distance from reference path
    double KV = 1.0;                        // Target speed
    double KLAT = 1.0;                      // Lateral
    double KLON = 1.0;                      // Longitudinal

    common::math::CartesianFrenetConverter cartesian_frenet_converter_;
    frenet_trajectory_info::FrenetTrajectory frenet_trajectory_;
    std::list<frenet_trajectory_info::FrenetTrajectory> frenet_trajectory_all_;
    std::list<frenet_trajectory_info::FrenetTrajectory> frenet_trajectory_ok_;

    // Trajectory1DBundle ptr_lon_trajectory_bundle_;
    

    /* FUNCTION */  

    void calc_frenet_trajectories(std::array<double, 3> &S0, std::array<double, 3> &L0);
    void calc_global_trajectories(std::list<frenet_trajectory_info::FrenetTrajectory> &frenet_trajectory_all, spline_2d::Spline2D &reference_path);
    void check_trajectories(std::list<frenet_trajectory_info::FrenetTrajectory> &frenet_trajectory_all, std::vector<std::vector<double>> &obstacle);
    bool check_collision(const frenet_trajectory_info::FrenetTrajectory &ft, std::vector<std::vector<double>> &obstacle);

    /* 生成纵向轨迹束 */
    void GenerateLongitudinalTrajectoryBundle(const std::array<double, 3> &S0, const double &ref_cruise_speed, Trajectory1DBundle *ptr_lon_trajectory_bundle);
    void GenerateLateralTrajectoryBundle(const std::array<double, 3> &L0, Trajectory1DBundle *ptr_lat_trajectory_bundle);
    std::vector<Condition> SampleLonEndConditionsForCruising(const std::array<double, 3> &S0, const double &ref_cruise_speed);
    std::vector<Condition> SampleLatEndConditions();



public:
    LatticePlanner(/* args */);
    ~LatticePlanner();

/**
 * @brief lattice planner
 * 
 * @param S0 : 初始S坐标
 * @param L0 : 初始L坐标
 * @param reference_path : 参考轨迹
 * @param obstacle : 障碍物信息
 * @param best_trajectory : 规划出的最优轨迹
 * 
 * @retval 是否成功规划出轨迹
 */
    bool lattice_planning(std::array<double, 3> &S0, 
                          std::array<double, 3> &L0,
                          spline_2d::Spline2D &reference_path,
                          std::vector<std::vector<double>> &obstacle,
                          frenet_trajectory_info::FrenetTrajectory &best_trajectory);


    bool PlanOnReferenceLine(const std::array<double, 3> &S0, const std::array<double, 3> &L0, const double &planning_target, spline_2d::Spline2D &reference_line,
                            std::vector<TrajectoryPoint> &best_trajectory);


};


} // namespace lp

#endif