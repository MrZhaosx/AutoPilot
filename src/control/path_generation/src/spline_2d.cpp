#include "spline_2d.h"

Spline2D::Spline2D(std::vector<double> &X, std::vector<double> &Y)
{
    calc_s(X,Y,s_);
    spline_x_.set_points(s_,X);
    spline_y_.set_points(s_,Y);

}

Spline2D::~Spline2D()
{
}

void Spline2D::calc_s(std::vector<double> &x, std::vector<double> &y, std::vector<double> &s){
    double dx, dy, ds;
    s.push_back(0.0);
    ds = 0;
    for (size_t i = 0; i < x.size()-1; i++){
        dx = x[i+1] - x[i];
        dy = y[i+1] - y[i];
        ds = ds + std::sqrt(dx*dx + dy*dy);
        s.push_back(ds);
    }
}

double Spline2D::calc_k(double s){
    double dx = spline_x_.deriv(1,s);
    double ddx = spline_x_.deriv(2,s);
    double dy = spline_y_.deriv(1,s);
    double ddy = spline_y_.deriv(2,s);

    return (ddy * dx - ddx * dy ) / (dx*dx + dy*dy);
}

double Spline2D::calc_yaw(double s){
    double dx = spline_x_.deriv(1,s);
    double dy = spline_y_.deriv(1,s);
    return atan2(dy,dx);
}


std::vector<double> Spline2D::calc_pos(double s){
    std::vector<double> pos;
    pos.push_back(spline_x_(s));
    pos.push_back(spline_y_(s));

    return pos;
}
