#include <cstdio>
#include <vector>
#include "spline.h"
#include "spline_2d.h"
#include <ros/ros.h>
#include <fstream>
#include <iostream>

using namespace std;

//  -77.563076  -73.532663  -67.246498 -61.315024   -47.236137  -33.460548  -22.338334  -11.673361    0.745452      7.114913        15.401369   22.758118   28.832259
//  -195.777909 -210.562602 -225.531035 -232.134871 -238.191930 -238.771780 -236.139608 -233.099172   -229.780873   -223.621706     -215.674612 -206.079242 -194.232519

// -81.25   -80.28  -77.87  -74.65  -71.44
// -26.33   -21.77  -18.55  -14.74  -13.09

// 12.7 18.96   24.00   28.31   31.50   32.36
// -9.6 -12.26  -15.75  -20.10  -24.83  -32.0
std::ofstream outfile;

int main(int, char**) {
//    std::vector<double> X = {34.173445, 34.022358, 32.669174, 20.312979, -15.547631, -38.144959, -64.011852, -79.940615, -81.109188, -80.664377, -64.438278, 0.082621}; // must be increasing
//    std::vector<double> Y = {-28.731891, -104.188746, -181.837540, -216.552377, -233.629654, -238.839495, -230.614931, -187.902293, -125.478381, -22.102643, -9.736178, -9.632866};

   std::vector<double> X = {-81.45, -77.563076, -73.532663 ,-67.246498 ,-61.315024 ,  -47.236137,  -33.460548,  -22.338334,  -11.673361,    0.745452,      7.114913,        15.401369,   22.758118,   28.832259,32.36}; // must be increasing
   std::vector<double> Y = {-179.73,-195.777909,-210.562602,-225.531035 ,-232.134871 ,-238.191930, -238.771780, -236.139608, -233.099172,   -229.780873,   -223.621706,     -215.674612, -206.079242, -194.232519,-179.73};

    std::vector<double> X1 = {-81.25,   -80.28,  -77.87,  -74.65,  -71.44};
    std::vector<double> Y1 = {-26.33,   -21.77,  -18.55,  -14.74,  -13.09};

    std::vector<double> X2 = {4.8,12.7, 18.96,   24.00,   28.31,   31.50,   32.36};
    std::vector<double> Y2 = {-9.0,-9.6, -12.26,  -15.75,  -20.10,  -24.83,  -32.0};

    // 获取当前时间，精确到秒
    time_t currentTime = std::time(NULL);
    char chCurrentTime[64];
    string saveFilePath = "/home/linux/work/dataset/path/";
    std::strftime(chCurrentTime, sizeof(chCurrentTime), "%Y-%m-%d-%H-%M-%S", std::localtime(&currentTime)); //年月日 时分秒
    std::string stCurrentTime = chCurrentTime;     

    outfile.open(saveFilePath + "path" + "-" + stCurrentTime + ".csv", ios::trunc);
    if(!outfile.is_open())
    {
        cout << "fail to open!" << endl;
        return -1;
    }

    double x = 0;
    double y = 0;

    // 第1段
    tk::spline s2(X2,Y2);
    x = 4.8;
    while (x <= 32.36)
    {
        y=s2(x);
        outfile << setiosflags(ios::fixed) << setprecision(7) 
            << x << "\t"
            << y << endl;
        x = x + 0.5;
    }
    // 第2段
    // x = 32.36;
    // y = -32.0;
    while (y >= -178.49)
    {
        outfile << setiosflags(ios::fixed) << setprecision(7) 
            << x << "\t"
            << y << endl;
        y = y - 1.0;
    }
    // 第三段
    tk::spline s(X,Y);
    while (x >= -81.45)
    {
        y=s(x);
        outfile << setiosflags(ios::fixed) << setprecision(7) 
            << x << "\t"
            << y << endl;
        x = x - 0.5;
    }
    // 第四段
    while (y <= -32.0)
    {
        outfile << setiosflags(ios::fixed) << setprecision(7) 
            << x << "\t"
            << y << endl;
        y = y + 1.0;
    }


    tk::spline s1(X1,Y1);
    // 第5段
    while (x <= -68.77)
    {
        y=s1(x);
        outfile << setiosflags(ios::fixed) << setprecision(7) 
            << x << "\t"
            << y << endl;
        x = x + 0.5;
    }

    // 第6段
    while (x <= 4.8)
    {
        outfile << setiosflags(ios::fixed) << setprecision(7) 
            << x << "\t"
            << y << endl;
        x = x + 1.0;
    }

    return 0;
}