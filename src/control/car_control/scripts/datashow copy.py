#!/usr/bin/env python

from matplotlib import pyplot as plt
import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray


# *************** global variable ***************
odom_x_now = 1.0
odom_y_now = 1.0
odom_x_last = 0.5
odom_y_last = 0.5

show_multi_array = []
flag_show_multi_array = False
# *************** callback function ***************
def odom_cb(data):
    global odom_x_now
    global odom_y_now
    odom_x_now = data.pose.pose.position.x
    odom_y_now = data.pose.pose.position.y

# data:
#   0 -- current_state_.yaw
#   1 -- cmd_steer
#   2 -- theta_fai
#   3 -- theta_y
#   4 -- current_state_.v
#   5 -- target_speed_
#   6 -- control_cmd_.throttle
#   7 -- control_cmd_.brake

def show_multi_array_cb(data):
    global show_multi_array
    global flag_show_multi_array
    show_multi_array = data.data
    flag_show_multi_array = True

if __name__=="__main__":

    global odom_x_now
    global odom_y_now
    global odom_x_last
    global odom_x_last
    global show_multi_array
    global flag_show_multi_array

    show_multi_array_x_axis = 0
    cureent_speed = 0
    cmd_throttle = 0
    cmd_brake = 0

    plt.ion()
    fig = plt.figure(num=1)
    ax1 = fig.add_subplot(111)
    fig = plt.figure(num=2)
    ax21 = fig.add_subplot(211)
    ax22 = fig.add_subplot(212)
    
    rospy.Subscriber("/base_pose_ground_truth", Odometry, odom_cb)
    rospy.Subscriber("/show/multi/array", Float32MultiArray, show_multi_array_cb)

    rate = rospy.Rate(10) # 10hz
    
    while not rospy.is_shutdown():

        # *************** plot odom ***************
        if (odom_x_now-odom_x_last)**2 + (odom_y_now-odom_y_last)**2 > 0.01 :
            ax1.plot([odom_x_last, odom_x_now], [odom_y_last, odom_y_now],'r') 
            
        odom_x_last = odom_x_now
        odom_y_last = odom_y_now
        
        # *************** plot speed ***************
        if flag_show_multi_array :
            flag_show_multi_array = False
            ax21.plot([show_multi_array_x_axis, show_multi_array_x_axis+1],[show_multi_array[5], show_multi_array[5]],'r')
            ax21.plot([show_multi_array_x_axis, show_multi_array_x_axis+1],[cureent_speed, show_multi_array[4]],'g')
            
            ax22.plot([show_multi_array_x_axis, show_multi_array_x_axis+1],[cmd_throttle, show_multi_array[6]],'g')
            ax22.plot([show_multi_array_x_axis, show_multi_array_x_axis+1],[cmd_brake, show_multi_array[7]],'r')

            show_multi_array_x_axis = show_multi_array_x_axis + 1
            cureent_speed = show_multi_array[4]
            cmd_throttle = show_multi_array[6]
            cmd_brake  = show_multi_array[7]

        plt.pause(0.001)
        rate.sleep()
        

