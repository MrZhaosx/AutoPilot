# AutoPilot

#### 介绍
自动驾驶GAZEBO仿真环境

#### 软件架构
car_demo : 车辆模型以及仿真环境

control ： 车辆控制算法  stanley_controller


#### 使用说明 

1、启动仿真环境
```
roslaunch car_demo spawn_car.launch
```
2、启动控制节点

```
roslaunch car_control car_control.launch
```
3、使用rviz ** 2D Nav Goal** 触发控制节点运行



