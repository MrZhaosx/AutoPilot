#ifndef trajectory_info_h
#define trajectory_info_h

#include <iostream>

namespace lp
{
    
struct PathPoint
{
  // coordinates
  double x ;
  double y ;
  double z ;

  // direction on the x-y plane
  double theta;
  // curvature on the x-y planning
  double kappa;
  // accumulated distance from beginning of the path
  double s;
  double ds;
  double dds;

  // derivative of kappa w.r.t s.
  double dkappa;
  // derivative of derivative of kappa w.r.t s.
  double ddkappa;
  // The lane ID where the path point is on
  int lane_id ;

  // derivative of x and y w.r.t parametric parameter t in CosThetareferenceline
  double x_derivative;
  double y_derivative;
};

struct TrajectoryPoint
{
  // path point
  PathPoint path_point;
  // linear velocity
  double v ;  // in [m/s]
  // linear acceleration
  double a ;
  // relative time from beginning of the trajectory
  double relative_time ;
  // longitudinal jerk
  double da ;

};


} // namespace lp

#endif