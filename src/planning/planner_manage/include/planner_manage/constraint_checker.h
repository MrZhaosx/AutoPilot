#ifndef constraint_checker_h
#define constraint_checker_h

#include <iostream>
#include <vector>
#include <list>
#include "spline_2d.h"
#include "common_config.h"
#include "trajectory_info.h"

namespace lp
{
    class ConstraintChecker {
    public:
    enum class Result {
        VALID,
        LON_VELOCITY_OUT_OF_BOUND,
        LON_ACCELERATION_OUT_OF_BOUND,
        LON_JERK_OUT_OF_BOUND,
        LAT_VELOCITY_OUT_OF_BOUND,
        LAT_ACCELERATION_OUT_OF_BOUND,
        LAT_JERK_OUT_OF_BOUND,
        CURVATURE_OUT_OF_BOUND,
    };

    ConstraintChecker() = delete;
    static Result ValidTrajectory(const std::vector<TrajectoryPoint>& trajectory);
    
    };

} // namespace lp


#endif