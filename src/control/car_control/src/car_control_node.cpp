#include "car_control/car_control_node.h"



ControlBase::ControlBase(const ros::NodeHandle &nh, const ros::NodeHandle &nh_private)
    : nh_(nh),
      nh_private_(nh_private)
{
    flag_receive_odom_ = false;
    flag_have_goal_ = false;

    double gain_k_stanley_controller = 0, gain_d_stanley_controller = 0, kp_stanley_controller = 0;
    nh_private_.param<double>("gain_k_stanley_controller", gain_k_stanley_controller, 1.0);
    nh_private_.param<double>("gain_d_stanley_controller", gain_d_stanley_controller, 1.0);
    nh_private_.param<double>("kp_stanley_controller", kp_stanley_controller, 1.0);

    nh_private_.param<std::string>("filePath", file_path_, " ");
    nh_private_.param<double>("speed_max", speed_max_, 37.998);
    nh_private_.param<double>("steer_max", steer_max_, 0.6458);

    nh_private_.param<double>("kp", pid_speed_.kp, 1.0);
    nh_private_.param<double>("ki", pid_speed_.ki, 0.0);
    nh_private_.param<double>("kd", pid_speed_.kd, 0.0);
    nh_private_.param<double>("threshold_throttle_break", threshold_throttle_break_, 0.05);
    nh_private_.param<double>("target_speed", target_speed_, 10.0);


#ifdef NDEBUG
    show_multi_array_.data.resize(10);
#endif

    // 创建 StanleyController
    stanley_controller_.reset(new StanleyController(gain_k_stanley_controller,gain_d_stanley_controller,kp_stanley_controller,steer_max_,speed_max_));


}

ControlBase::~ControlBase()
{
}

void ControlBase::init(){

    cmdLoop_timer_ = nh_.createTimer(ros::Duration(0.01), &ControlBase::cmdLoop_cb,
                                   this); // Define timer for constant loop rate
    control_cmd_pb_ = nh_.advertise<prius_msgs::Control>("/prius", 1);
    ref_path_rviz_pb_ = nh_.advertise<nav_msgs::Path>("/reference/path", 1);

    car_odom_sb_ = nh_.subscribe<nav_msgs::Odometry>("/prius/odom", 1, &ControlBase::carOdom_cb, this);
    // goal_sb_ = nh_.subscribe<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, &ControlBase::goal_cb, this);
    planner_cmd_sb_ = nh_.subscribe<common_msgs::PlannerCommand>("/planner/command", 1, &ControlBase::planner_cmd_cb, this);
    
#ifdef NDEBUG
    debug_target_velocity_pb_ = nh_.advertise<geometry_msgs::PoseStamped>("/debug/target/velocity", 1);
    show_multi_array_pb_ =  nh_.advertise<std_msgs::Float32MultiArray>("/show/multi/array", 1);
#endif
}

bool ControlBase::LoadPath(std::string file_path, std::vector<std::array<double,2>> &data){
	std::ifstream in;
    geometry_msgs::PoseStamped this_pose_stamped; 
	in.open(file_path, std::ios::in);
	if (!in.is_open()){
        std::cout << "File Path : " << file_path << std::endl;
		std::cout << "open File Failed." << std::endl;
		return false;
	}
	std::string strOne;
	std::array<double,2> vec;
	while (getline(in, strOne)){
		std::stringstream ss;
		ss << strOne;
		char c;
		ss >> vec[0] >> c >> vec[1];
		data.push_back(vec);

        this_pose_stamped.pose.position.x = vec[0]; 
		this_pose_stamped.pose.position.y = vec[1]; 
        this_pose_stamped.pose.position.z = 0.1;
        ref_path_rviz_.poses.push_back(this_pose_stamped);
	}
	return true;
}

void ControlBase::carOdom_cb(const nav_msgs::Odometry::ConstPtr &odomMsg){

    current_state_.x = odomMsg->pose.pose.position.x;
    current_state_.y = odomMsg->pose.pose.position.y;

    tf::Quaternion quat;
    tf::quaternionMsgToTF(odomMsg->pose.pose.orientation, quat);
    double roll, pitch, yaw;
    tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);//进行转换
    current_state_.yaw = yaw;

    current_state_.v = std::sqrt(std::pow(odomMsg->twist.twist.linear.x, 2) + std::pow(odomMsg->twist.twist.linear.y, 2));

    if( !flag_receive_odom_) flag_receive_odom_ = true;
}

void ControlBase::goal_cb(const geometry_msgs::PoseStamped::ConstPtr& msg){
    flag_have_goal_ = true;
}

void ControlBase::planner_cmd_cb(const common_msgs::PlannerCommand::ConstPtr &msg){
    // planner_cmd_ = *msg;
    ref_path_.clear();
    std::array<double,4> ref_path;
    for (size_t i = 0; i < msg->pose2d.size(); i++){
        ref_path[0] = msg->pose2d[i].x;
        ref_path[1] = msg->pose2d[i].y;
        ref_path[2] = msg->pose2d[i].theta;
        ref_path[3] = msg->velocity[i];
        ref_path_.push_back(ref_path);
    }
    flag_receive_cmd_ = true;

    
}

void ControlBase::cmdLoop_cb(const ros::TimerEvent &event){

    static int fsm_num = 0;

    switch (exec_state_){
        case INIT :{
            if( !flag_receive_odom_){
                fsm_num++;
                if (fsm_num == 100) {
                    printExecState();
                    fsm_num = 0;
                    std::cout << "no odom !!! " << std::endl;
                }
                return;
            }
            changeExecState(WAIT_TARGET);
            break;
        }
        case WAIT_TARGET :{
            fsm_num++;
            if (fsm_num == 100) {
                printExecState();
                fsm_num = 0;
                ref_path_rviz_.header.stamp = ros::Time::now();
                ref_path_rviz_.header.frame_id = "map";
                ref_path_rviz_pb_.publish(ref_path_rviz_);
            }
            if( !flag_receive_cmd_){
                return;
            }
            changeExecState(EXEC_TRAJ);
            break;
        }
        case FIRST_EXEC_TRAJ :{
            break;
        }
        case EXEC_TRAJ :{


            std::vector<double> current_state_vector;
            current_state_vector.push_back(current_state_.x);
            current_state_vector.push_back(current_state_.y);
            current_state_vector.push_back(current_state_.yaw);
            current_state_vector.push_back(current_state_.v);
            size_t index_last = stanley_controller_->getMinDisIndex();
            stanley_controller_->findTargetIdx(ref_path_,current_state_vector);
            computeControlCmd(ref_path_);

            if (ref_path_.size() - index_last < 5)
                changeExecState(STOP);

        #ifdef NDEBUG
            // std::cout << " pid_speed_.err :" << pid_speed_.err << std::endl;

            size_t index = stanley_controller_->getMinDisIndex();
            debug_target_velocity_.pose.position.x = ref_path_[std::min(index + 5,ref_path_.size()-1)][0];
            debug_target_velocity_.pose.position.y = ref_path_[std::min(index + 5,ref_path_.size()-1)][1];
            debug_target_velocity_.pose.position.z = 0.1;
            debug_target_velocity_.header.stamp = ros::Time::now();
            debug_target_velocity_.header.frame_id = "map";
            debug_target_velocity_pb_.publish(debug_target_velocity_);

            show_multi_array_.data[0] = current_state_.yaw *180/M_PI;
            show_multi_array_.data[2] = stanley_controller_->theta_fai *180/M_PI;
            show_multi_array_.data[3] = stanley_controller_->theta_y *180/M_PI;
            show_multi_array_.data[4] = current_state_.v;
            show_multi_array_.data[5] = target_speed_;
            show_multi_array_.data[6] = control_cmd_.throttle;
            show_multi_array_.data[7] = control_cmd_.brake;
            show_multi_array_pb_.publish(show_multi_array_);
        #endif

            break;
        }
        case STOP :{
            if(stop()){
                flag_receive_cmd_ = false;
                changeExecState(WAIT_TARGET);
            }
            break;
        }
        default:
            break;
    }

    control_cmd_.header.stamp = ros::Time::now();
    control_cmd_pb_.publish(control_cmd_);
}

void ControlBase::computeControlCmd(const std::vector<std::array<double,4>> &ref_path){

    std::vector<double> current_state_vector;
    std::vector<double> cmd;

    current_state_vector.push_back(current_state_.x);
    current_state_vector.push_back(current_state_.y);
    current_state_vector.push_back(current_state_.yaw);
    current_state_vector.push_back(current_state_.v);

    stanley_controller_->ComputeControlCommand(ref_path,current_state_vector,cmd);
    
    // 转向
    control_cmd_.steer = std::max(-1.0, std::min(cmd[0] / steer_max_, 1.0)); // 归一化

    // 油门控制

    /*  考虑到一般情况下速度不会突然由负极大值变为正极大值，因此除以speed_max_归一化
        std::abs()保证前进与后退时速度误差方向保持一致 */
    pid_speed_.err = (std::abs(ref_path[stanley_controller_->getMinDisIndex()][3]) - std::abs(current_state_.v)); 
    std::cout << " cmd speed :" << ref_path[stanley_controller_->getMinDisIndex()][3] << std::endl;
    pid_speed_.out = pid_speed_.kp * pid_speed_.err + pid_speed_.ki * pid_speed_.err_i + pid_speed_.kd * (pid_speed_.err - pid_speed_.err_last);
    pid_speed_.err_last = pid_speed_.err;
    pid_speed_.err_i = pid_speed_.err_i + pid_speed_.err;

    pid_speed_.out = std::max(-1.0, std::min(pid_speed_.out, 1.0));
    std::cout << "pid_speed_.out :                " << pid_speed_.out << std::endl;
    if(pid_speed_.out > std::abs(threshold_throttle_break_ - 0.03)){ // 刹车与油门控制
        control_cmd_.throttle = std::abs(pid_speed_.out);
        control_cmd_.brake = 0.0;
    }else if(pid_speed_.out < -std::abs(threshold_throttle_break_)){
        control_cmd_.brake = std::abs(pid_speed_.out);
        control_cmd_.throttle = 0.0;
    }else{
        control_cmd_.throttle = 0.0;
        control_cmd_.brake = 0.0;
    }

    if(ref_path[stanley_controller_->getMinDisIndex()][3] > 0)
        control_cmd_.shift_gears = prius_msgs::Control::FORWARD;
    else
        control_cmd_.shift_gears = prius_msgs::Control::REVERSE;

#ifdef NDEBUG
    debug_target_velocity_.pose.orientation = tf::createQuaternionMsgFromYaw(cmd[0]+current_state_.yaw);
    show_multi_array_.data[1] = cmd[0] * 180/M_PI;
#endif

}

void ControlBase::computeControlCmd(){
    std::vector<double> current_state_vector;
    std::vector<double> cmd;

    current_state_vector.push_back(current_state_.x);
    current_state_vector.push_back(current_state_.y);
    current_state_vector.push_back(current_state_.yaw);
    current_state_vector.push_back(current_state_.v);

    stanley_controller_->ComputeControlCommand(ref_path_,current_state_vector,cmd);
    
    // 转向
    control_cmd_.steer = std::max(-1.0, std::min(cmd[0] / steer_max_, 1.0)); // 归一化

    // 油门控制

    /*  考虑到一般情况下速度不会突然由负极大值变为正极大值，因此除以speed_max_归一化
        std::abs()保证前进与后退时速度误差方向保持一致 */
    pid_speed_.err = (std::abs(target_speed_) - std::abs(current_state_.v)); 
    pid_speed_.out = pid_speed_.kp * pid_speed_.err + pid_speed_.ki * pid_speed_.err_i + pid_speed_.kd * (pid_speed_.err - pid_speed_.err_last);
    pid_speed_.err_last = pid_speed_.err;
    pid_speed_.err_i = pid_speed_.err_i + pid_speed_.err;

    pid_speed_.out = std::max(-1.0, std::min(pid_speed_.out, 1.0));
    
    if(pid_speed_.out > std::abs(threshold_throttle_break_ - 0.03)){ // 刹车与油门控制
        control_cmd_.throttle = std::abs(pid_speed_.out);
        control_cmd_.brake = 0.0;
    }else if(pid_speed_.out < -std::abs(threshold_throttle_break_)){
        control_cmd_.brake = std::abs(pid_speed_.out);
        control_cmd_.throttle = 0.0;
    }else{
        control_cmd_.throttle = 0.0;
        control_cmd_.brake = 0.0;
    }

    if(cmd[1] > 0)
        control_cmd_.shift_gears = prius_msgs::Control::FORWARD;
    else
        control_cmd_.shift_gears = prius_msgs::Control::REVERSE;

#ifdef NDEBUG
    debug_target_velocity_.pose.orientation = tf::createQuaternionMsgFromYaw(cmd[0]+current_state_.yaw);
    show_multi_array_.data[1] = cmd[0] * 180/M_PI;
#endif

}

bool ControlBase::stop(){
    if(std::abs(current_state_.v) > 0.01){
        control_cmd_.throttle = 0.0;
        control_cmd_.brake = 0.3;
        return false;
    }else{
        control_cmd_.throttle = 0.0;
        control_cmd_.brake = 0.0;
        control_cmd_.shift_gears = prius_msgs::Control::NEUTRAL;
    }
    return true;
}

void ControlBase::changeExecState(EXEC_STATE new_state){
    std::string state_str[5] = { "INIT", "WAIT_TARGET", "FIRST_EXEC_TRAJ", "EXEC_TRAJ", "STOP" };
    int  pre_s        = int(exec_state_);
    exec_state_         = new_state;
    std::cout << "ExecState : from " + state_str[pre_s] + " to " + state_str[int(new_state)] << std::endl;
}

void ControlBase::printExecState(){
    std::string state_str[5] = { "INIT", "WAIT_TARGET", "FIRST_EXEC_TRAJ", "EXEC_TRAJ", "STOP" };
    std::cout << "ExecState : " + state_str[int(exec_state_)] << std::endl;
}

int main(int argc, char** argv){
    ros::init(argc, argv, "car_control_node");
    ros::NodeHandle nh("");
    ros::NodeHandle nh_private("~");

    ControlBase *control_base = new ControlBase(nh, nh_private);
    control_base->init();

    ros::spin();

    delete control_base;

    return 0;
}