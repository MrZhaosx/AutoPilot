#ifndef PiecewiseAccelerationTrajectory1d_h
#define PiecewiseAccelerationTrajectory1d_h

#include <iostream>
#include <algorithm>
#include <array>
#include <string>
#include <vector>
#include <cassert>
#include "polynomial_calculation.h"
#include "common_config.h"

namespace lp
{

template <typename T>
T lerp(const T &x0, const double t0, const T &x1, const double t1,
       const double t) {
  if (std::abs(t1 - t0) <= 1.0e-6) {
    std::cout << "input time difference is too small! t0: " << t0 << "  t1:  " << t1 << std::endl;
    return x0;
  }
  const double r = (t - t0) / (t1 - t0);
  const T x = x0 + r * (x1 - x0);
  return x;
}

class PiecewiseAccelerationTrajectory1d : public polynomial::PolynomialCurve1d {
 public:
  PiecewiseAccelerationTrajectory1d(const double start_s, const double start_v);

  virtual ~PiecewiseAccelerationTrajectory1d() = default;

  void AppendSegment(const double a, const double t_duration);

  void PopSegment();

  double ParamLength() const override;

//   std::string ToString() const override;

  double get_deriv(int order, double x) const override ;
  double get_y(double x) const override ;

  std::array<double, 4> Evaluate(const double t) const;

 private:
  double Evaluate_s(const double t) const;

  double Evaluate_v(const double t) const;

  double Evaluate_a(const double t) const;

  double Evaluate_j(const double t) const;

 private:
  // accumulated s
  std::vector<double> s_;

  std::vector<double> v_;

  // accumulated t
  std::vector<double> t_;

  std::vector<double> a_;
};
} // namespace lp


#endif